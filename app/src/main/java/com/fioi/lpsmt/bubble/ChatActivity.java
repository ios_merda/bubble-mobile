package com.fioi.lpsmt.bubble;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.helpers.chat.ChatAdapter;
import com.fioi.lpsmt.bubble.helpers.chat.PostChatRepository;
import com.fioi.lpsmt.bubble.model.Message;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "CHAT_ACTIVITY";

    private String postId;
    private String postTitle;

    private String userId;
    private PostChatRepository postChatRepository;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    private EditText messageEditText;
    private FloatingActionButton send;
    private ProgressBar chatProgressBar;
    private TextView stillNoMessages;

    private UserPreferences userPreferences;

    private RecyclerView chatRecycler;
    private ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        Bundle extras = getIntent().getExtras();
        if(extras != null){
            postId = extras.getString("POST_ID");
            postTitle = extras.getString("POST_TITLE");
        }
        setupActionBar();

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        postChatRepository = new PostChatRepository(FirebaseFirestore.getInstance());

        if(mAuth.getCurrentUser() != null){
            userId = mAuth.getCurrentUser().getUid();
        }else{
            userNotAuthenticated();
        }

        chatProgressBar = findViewById(R.id.chatProgressBar);
        chatProgressBar.setVisibility(View.GONE);
        stillNoMessages = findViewById(R.id.still_no_message_here);
        stillNoMessages.setVisibility(View.GONE);

        userPreferences = new UserPreferences(this);

        Slidr.attach(this, configSlidr());
        initUI();
        showAllPostMessages();

    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        if(isTaskRoot()){
                            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            if(inputManager != null && getCurrentFocus() != null){
                                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            }
                            startActivity(new Intent(ChatActivity.this, MainActivity.class));
                            finish();
                        }else{
                            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            if(inputManager != null && getCurrentFocus() != null){
                                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            }
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    }
                }).build();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        assert intent.getExtras() != null;
        postId = intent.getExtras().getString("POST_ID");
        postTitle = intent.getExtras().getString("POST_TITLE");
        setTitle(postTitle);
        initUI();
        chatProgressBar.setVisibility(View.VISIBLE);
        showAllPostMessages();
        chatProgressBar.setVisibility(View.INVISIBLE);
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            if(isTaskRoot()){
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(inputManager != null && getCurrentFocus() != null){
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                startActivity(new Intent(ChatActivity.this, MainActivity.class));
                finish();
            }else{
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(inputManager != null && getCurrentFocus() != null){
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                setResult(RESULT_CANCELED);
                finish();
            }

        });
        setTitle(postTitle);
    }


    private void initUI(){
        messageEditText = findViewById(R.id.message_text);
        send = findViewById(R.id.send_message);
        chatRecycler = findViewById(R.id.chats);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(true);
        chatRecycler.setLayoutManager(manager);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(messageEditText.getText().toString().isEmpty()){
                    Toast.makeText(ChatActivity.this, getString(R.string.enter_a_message), Toast.LENGTH_LONG).show();
                }else{
                    sendMessage();
                }
            }
        });
    }

    /**
     * Handles message sent event.
     */
    private void sendMessage() {
        String message = messageEditText.getText().toString();
        messageEditText.setText("");
        send.setEnabled(false);
        postChatRepository.sendMessage(
                postId,
                userId,
                message,
                userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_NAME),
                userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_IMAGE),
                postTitle,
                new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        stillNoMessages.setVisibility(View.INVISIBLE);
                        send.setEnabled(true);
                        Map<String, Object> notificationData = new HashMap<>();
                        notificationData.put("postId", postId);
                        notificationData.put("postTitle", postTitle);
                        notificationData.put("userId", userId);
                        notificationData.put("userName", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_NAME));
                        notificationData.put("userImageUri", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_IMAGE));
                        notificationData.put("messageId", documentReference.getId());
                        notificationData.put("messageText", message);
                        db.collection("post_members").document(postId).update("members", FieldValue.arrayUnion(userId)).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "Successfully added new member to chat room");
                                db.collection("notifications").add(notificationData).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Log.d(TAG, "Successfully added new notification");

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d(TAG, "Failed to create new notification");
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "Failed to add new member to chat room", e);
                            }
                        });
                    }
                }, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        send.setEnabled(true);
                        Toast.makeText(ChatActivity.this, getString(R.string.failed_to_send_message), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                });
    }


    /**
     * Gets all messages of a chatroom.
     */
    private void showAllPostMessages(){
        postChatRepository.getPostMessages(postId, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Log.e(TAG, "Listener Failed", e);
                    return;
                }

                if(queryDocumentSnapshots.isEmpty())
                    stillNoMessages.setVisibility(View.VISIBLE);

                List<Message> messages = new ArrayList<>();
                assert queryDocumentSnapshots != null;
                for(QueryDocumentSnapshot documentSnapshot: queryDocumentSnapshots){
                    Message message = new Message(
                            documentSnapshot.getId(),
                            documentSnapshot.getString("post"),
                            documentSnapshot.getString("sender"),
                            documentSnapshot.getString("text"),
                            documentSnapshot.getLong("sent")
                    );
                    message.setSenderName(documentSnapshot.getString("senderName"));
                    String senderImageString = documentSnapshot.getString("senderImageUrl");
                    if(senderImageString != null){
                        message.setSenderImage(Uri.parse(senderImageString));
                    }
                    messages.add(message);
                    
                    Log.d(TAG, documentSnapshot.getData().toString());
                }
                Log.d(TAG, Integer.toString(messages.size()));
                adapter = new ChatAdapter(userId, messages);
                chatRecycler.setAdapter(adapter);

            }
        });
    }

    private void userNotAuthenticated(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
