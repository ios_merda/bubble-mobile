package com.fioi.lpsmt.bubble.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles post deletion
 */
public class PostDeleteScheduler {
    private List<String> deletedPostIds;

    public PostDeleteScheduler(){
        setDeletedPostIds(new ArrayList<>());
    }


    public List<String> getDeletedPostIds() {
        return deletedPostIds;
    }

    public void setDeletedPostIds(List<String> deletedPostIds) {
        this.deletedPostIds = deletedPostIds;
    }
}
