package com.fioi.lpsmt.bubble.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.fioi.lpsmt.bubble.R;

/**
 * Dialog prompting user to enter password in order to link accounts.
 */
public class PasswordLinkDialog extends AppCompatDialogFragment {
    private EditText passwordField;
    private TextView explanation;
    private PasswordLinkDialogListener passwordLinkDialogListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.password_link_dialog, null);

        builder.setView(view)
                .setTitle("Confirm Account")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = passwordField.getText().toString();
                passwordLinkDialogListener.returnPassword(password);
            }
        });

        passwordField = view.findViewById(R.id.pass_link_edit_text);
        explanation = view.findViewById(R.id.pass_link_explanation);
        explanation.setText(this.getString(R.string.facebook_link_explanation));

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            passwordLinkDialogListener = (PasswordLinkDialogListener) context;
        }catch(ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement PasswordLinkDialogListener");
        }
    }

    public interface PasswordLinkDialogListener{
        void returnPassword(String password);
    }
}
