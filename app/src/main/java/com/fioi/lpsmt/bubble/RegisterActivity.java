package com.fioi.lpsmt.bubble;

import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.helpers.Validator;
import com.fioi.lpsmt.bubble.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "REGISTER_ACTIVITY";
    public static final int REQUEST_GET_SINGLE_FILE = 1;
    private boolean AVATAR_CHOSEN = false;

    public EditText name;
    public EditText email;
    public TextInputLayout mail, pw, pwConf;
    public EditText editTextPassword;
    public EditText editTextConfirmPassword;
    public ImageView defaultImage;
    public Button reg;
    public ProgressBar progressBar;
    public User u;

    private UserPreferences userPreferences;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    private FirebaseStorage storage;
    private StorageReference storageRef, avatarsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(inputManager != null && getCurrentFocus() != null){
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            finish();
        });

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        storage = FirebaseStorage.getInstance("gs://bubble-1556982239730.appspot.com/");
        storageRef = storage.getReference();
        avatarsRef = storageRef.child("usersAvatars");

        reg = findViewById(R.id.btnRegister);
        reg.setEnabled(Boolean.FALSE);

        name= findViewById(R.id.txtName);
        name.addTextChangedListener(fieldsWatcher);

        email= findViewById(R.id.txtEmail);
        mail = findViewById(R.id.textMailInputLayout);
        pw = findViewById(R.id.textPasswordInputLayout);
        pwConf = findViewById(R.id.textPasswordConfInputLayout);
        email.addTextChangedListener(fieldsWatcher);
        email.addTextChangedListener(emailWatcher);


        editTextPassword= findViewById(R.id.txtPassword);
        editTextPassword.addTextChangedListener(passwStrengthTextEditorWatcher);
        editTextPassword.addTextChangedListener(passwMatchTextEditorWatcher);
        editTextPassword.addTextChangedListener(fieldsWatcher);

        editTextConfirmPassword= findViewById(R.id.txtConfirmPassw);
        editTextConfirmPassword.addTextChangedListener(passwMatchTextEditorWatcher);
        editTextConfirmPassword.addTextChangedListener(fieldsWatcher);

        defaultImage = findViewById(R.id.avatar);
        progressBar = findViewById(R.id.registerProgressBar);
        progressBar.setVisibility(View.GONE);
        userPreferences = new UserPreferences(this);
        u = new User();

        Slidr.attach(this, configSlidr());
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if(inputManager != null && getCurrentFocus() != null){
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        finish();
                    }
                }).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Implicit intent for selecting image from gallery.
     * @param view
     */
    public void pickImage(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GET_SINGLE_FILE);
    }

    /**
     * Activates when user returns to register activity with a result from other activities.
     * @param requestCode Request code of operation.
     * @param resultCode Result code of operation.
     * @param data Data passed from other activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_GET_SINGLE_FILE) {

                    Uri selectedImageUri = data.getData();

                    if (selectedImageUri != null){
                        u.setImageUri(selectedImageUri);
                        AVATAR_CHOSEN = true;
                    }

                    final String path = getPathFromURI(selectedImageUri);

                    if (path != null) {
                        File f = new File(path);
                        selectedImageUri = Uri.fromFile(f);
                    }

                    ImageView avatar = (ImageView) findViewById(R.id.avatar);
                    avatar.setImageURI(selectedImageUri);
                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    /**
     * Gets path as string from local uri of image.
     * @param contentUri Local uri of image
     * @return the path of the image.
     */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            cursor.close();
        }
        return res;
    }

    /**
     * Saves user profile to Firebase.
     * @param view
     */
    public void saveUser(View view) {
        progressBar.setVisibility(View.VISIBLE);
        EditText name = findViewById(R.id.txtName);
        EditText email = findViewById(R.id.txtEmail);
        EditText password =  findViewById(R.id.txtPassword);
        
        ImageView avatarView = findViewById(R.id.avatar);
        HashMap<String, Object> userMap = new HashMap<>();
        userMap.put("Name", name.getText().toString());
        userMap.put("Email", email.getText().toString());
        userMap.put("PhotoUrl", null);

        mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("asd", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // Add a new document with a generated ID
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    Log.d(TAG, "Successfully got instance id");
                                    userMap.put("deviceToken", instanceIdResult.getToken());
                                    db.collection("users").document(user.getUid())
                                            .set(userMap)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d("addUser", "DocumentSnapshot successfully written!");
                                                    // Get the data from an ImageView as bytes
                                                    if(AVATAR_CHOSEN){
                                                        Bitmap bitmap = ((BitmapDrawable) avatarView.getDrawable()).getBitmap();
                                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                                        byte[] data = baos.toByteArray();
                                                        StorageReference avatarRef = storageRef.child("usersAvatars/" + user.getUid());
                                                        UploadTask uploadTask = avatarRef.putBytes(data);
                                                        uploadTask.addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception exception) {
                                                                Log.e(TAG, "Image upload failed.");
                                                            }
                                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                            @Override
                                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                                Log.d(TAG, "Image uploaded successfully");
                                                                avatarRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                    @Override
                                                                    public void onSuccess(Uri uri) {
                                                                        db.collection("users").document(user.getUid()).update("PhotoUrl", uri.toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                            @Override
                                                                            public void onSuccess(Void aVoid) {
                                                                                Log.d(TAG, "Image url stored successfully");
                                                                                onRegistrationComplete();
                                                                                progressBar.setVisibility(View.INVISIBLE);
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }else{
                                                        onRegistrationComplete();
                                                        progressBar.setVisibility(View.INVISIBLE);
                                                    }

                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.w("addUser", "Error writing document", e);
                                                    progressBar.setVisibility(View.INVISIBLE);
                                                }
                                            });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG, "Failed to save device token");
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            });

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("asd", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, getString(R.string.error_authentication),
                                    Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                    }
                });

    }

    /**
     * Activates upon successful registration and redirects user to Main Activity.
     */
    private void onRegistrationComplete() {
        userPreferences.putString(UserPreferences.USER_AUTH_TYPE, "bubble.local");
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    private final TextWatcher fieldsWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }

        public void afterTextChanged(Editable s) {
            if(name.getText().toString().length()> 0
                    && email.getText().toString().length() > 0
                    && editTextPassword.getText().toString().length() > 0
                    && editTextConfirmPassword.getText().toString().length() > 0
                    && new Validator(Validator.EMAIL_PATTERN).validate(email.getText().toString())
                    && editTextConfirmPassword.getText().toString().equals(editTextPassword.getText().toString())){

                reg.setEnabled(true);

            }else{

                reg.setEnabled(false);

            }
        }
    };

    private final TextWatcher emailWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mail.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            mail.setError(!check ? getString(R.string.mail_not_valid) : null);
        }

        public void afterTextChanged(Editable s) {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            mail.setError(!check ? getString(R.string.mail_not_valid) : null);
        }
    };

    private final TextWatcher passwStrengthTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            pw.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s) {
            if (s.toString().isEmpty()) {
                pw.setError(getString(R.string.not_entered));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<6) {
                pw.setError(getString(R.string.easy));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<10) {
                pw.setError(getString(R.string.medium));
                pw.setErrorTextAppearance(R.style.MediumPasswordMessage);
            } else {
                pw.setError(getString(R.string.strong));
                pw.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }
    };

    private final TextWatcher passwMatchTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            //pwConf.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s)
        {
            if (!(editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) || editTextPassword.getText().toString().isEmpty() || editTextConfirmPassword.getText().toString().isEmpty()) {
                pwConf.setError(getString(R.string.password_no_match));
                pwConf.setErrorTextAppearance(R.style.ErrorMessage);
            } else {
                pwConf.setError(getString(R.string.password_match));
                pwConf.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }


        }
    };
}