package com.fioi.lpsmt.bubble.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Utility class for handling user preferences.
 */
public class UserPreferences {

    public static final String USER_PREFERENCES_FILE_NAME = "USER_PROFILE_DATA";
    public static final String USER_AUTH_TYPE = "AUTH_TYPE";

    public static final String SHARED_PREFERENCES_FACEBOOK_EMAIL = "FACEBOOK_EMAIL";
    public static final String SHARED_PREFERENCES_FACEBOOK_NAME = "FACEBOOK_NAME";
    public static final String SHARED_PREFERENCES_FACEBOOK_PHOTO_URL = "FACEBOOK_PHOTO_URL";
    public static final String SHARED_PREFERENCES_FACEBOOK_ID = "FACEBOOK_ID";

    public static final String SHARED_PREFERENCES_SEARCH_RADIUS = "SEARCH_RADIUS";

    public static final String SHARED_PREFERENCES_USER_NAME = "USER_NAME";
    public static final String SHARED_PREFERENCES_USER_IMAGE = "USER_IMAGE";

    public static final String SHARED_PREFERENCES_FIRST_START = "FIRST_START";

    private SharedPreferences preferences;

    public UserPreferences(Context context){
        this.preferences = context.getSharedPreferences(USER_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Returns value from preferences file.
     * @param key the key of the value to be returned.
     * @return the value as a String.
     */
    public String getString(String key){
        return preferences.getString(key, null);
    }

    /**
     * Inserts new String to preferences file as Key-Value pair.
     * @param key the key of the new String.
     * @param value the value of the new String.
     */
    public void putString(String key, String value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    /**
     * Returns value from preferences file.
     * @param key the key of the value to be returned.
     * @return the value as a Boolean.
     */
    public Boolean getBoolean(String key){
        return preferences.getBoolean(key, true);
    }

    /**
     * Inserts new Boolean to preferences file as Key-Value pair.
     * @param key the key of the new Boolean.
     * @param value the value of the new Boolean.
     */
    public void putBoolean(String key, boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    /**
     * Clears all data related to Facebook authentication.
     */
    public void clearFacebook(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(SHARED_PREFERENCES_FACEBOOK_EMAIL);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_NAME);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_PHOTO_URL);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_ID);
        editor.remove(SHARED_PREFERENCES_SEARCH_RADIUS);
        editor.apply();
    }

    /**
     * Clears all data that are relevant for single session.
     */
    public void clearOnLogout(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(SHARED_PREFERENCES_FACEBOOK_EMAIL);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_NAME);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_PHOTO_URL);
        editor.remove(SHARED_PREFERENCES_FACEBOOK_ID);
        editor.remove(SHARED_PREFERENCES_SEARCH_RADIUS);
        editor.remove(SHARED_PREFERENCES_USER_IMAGE);
        editor.remove(SHARED_PREFERENCES_USER_NAME);
        editor.apply();
    }

    /**
     * Clears all data in preferences file.
     */
    public void clear(){
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
