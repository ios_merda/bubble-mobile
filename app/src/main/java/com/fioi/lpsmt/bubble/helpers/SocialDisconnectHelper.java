package com.fioi.lpsmt.bubble.helpers;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public class SocialDisconnectHelper {

    /**
     * Handles logout from Facebook and clears related data from preferences.
     * @param userPreferences preferences to be cleared.
     */
    public static void disconnectFromFacebook(UserPreferences userPreferences){
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        LoginManager.getInstance().logOut();
        userPreferences.clearFacebook();
    }

    /**
     * Handles logout from Google
     * @param googleSignInClient
     */
    public static void disconnectFromGoogle(GoogleSignInClient googleSignInClient){
        googleSignInClient.signOut();
    }

}
