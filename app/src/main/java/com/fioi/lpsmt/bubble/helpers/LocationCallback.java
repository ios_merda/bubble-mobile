package com.fioi.lpsmt.bubble.helpers;


/**
 * Interface for enabling communication between Gps Broadcast receiver and Main Activity.
 */
public interface LocationCallback {
    void onLocationTriggered();
}
