package com.fioi.lpsmt.bubble.model;

import android.net.Uri;

public class Message{
    private String id;
    private String sender;
    private String post;
    private String text;
    private long sent;
    private String senderName;
    private Uri senderImage;
    private String postName;

    public Message(String id, String post, String sender, String text, long sent){
        this.setId(id);
        this.setSender(sender);
        this.setPost(post);
        this.setText(text);
        this.setSent(sent);
    }

    public Message(String postName, String senderName, String text, String post) {
        this.setPostName(postName);
        this.setSenderName(senderName);
        this.setText(text);
        this.setPost(post);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getSent() {
        return sent;
    }

    public void setSent(long sent) {
        this.sent = sent;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Uri getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(Uri senderImage) {
        this.senderImage = senderImage;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

}
