package com.fioi.lpsmt.bubble.recovery;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.LoginActivity;
import com.fioi.lpsmt.bubble.MainActivity;
import com.fioi.lpsmt.bubble.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class ResetPassword extends AppCompatActivity {
    private EditText passwordField;
    private EditText confirmPasswordField;
    private String code;
    public TextInputLayout pw, pwConf;
    private Button btnResetPassword;
    private ProgressBar resetPassProgressBar;


    private final Uri[] deepLink = {null};
    private final String[] user = new String[1];

    public static final String RESET_PASS_ERROR = "ERROR_MESSAGE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> startActivity(new Intent(getApplicationContext(),LoginActivity.class)));

        Intent intent = getIntent();
        Uri data = intent.getData();
        if(data != null){
            code = data.getQueryParameter("code");
        }


        passwordField = findViewById(R.id.password_recover_field);
        confirmPasswordField = findViewById(R.id.password_confirm_field);
        pw = findViewById(R.id.textNewPassRecovery);
        pwConf = findViewById(R.id.textNewPassConfRecovery);

        passwordField.addTextChangedListener(passwStrengthTextEditorWatcher);
        passwordField.addTextChangedListener(passwMatchTextEditorWatcher);
        confirmPasswordField.addTextChangedListener(passwMatchTextEditorWatcher);

        btnResetPassword = findViewById(R.id.reset_password_button);
        btnResetPassword.setEnabled(false);
        resetPassProgressBar = findViewById(R.id.resetPassProgressBar);
        resetPassProgressBar.setVisibility(View.GONE);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        if (pendingDynamicLinkData != null) {
                            deepLink[0] = pendingDynamicLinkData.getLink();
                            //System.out.println("dynamiclink: " + deepLink[0].toString());
                            user[0] = deepLink[0].getQueryParameter("oobCode");
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("dlfail", "getDynamicLink:onFailure", e);
                    }
                });
    }

    private final TextWatcher passwStrengthTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            pw.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s) {
            if (s.toString().isEmpty()) {
                pw.setError(getString(R.string.not_entered));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<6) {
                pw.setError(getString(R.string.easy));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<10) {
                pw.setError(getString(R.string.medium));
                pw.setErrorTextAppearance(R.style.MediumPasswordMessage);
            } else {
                pw.setError(getString(R.string.strong));
                pw.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }
    };

    private final TextWatcher passwMatchTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            pwConf.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s)
        {
            if (passwordField.getText().toString().equals(confirmPasswordField.getText().toString())) {
                pwConf.setError(getString(R.string.password_match));
                pwConf.setErrorTextAppearance(R.style.StrongPasswordMessage);

                if (passwordField.getText().toString().length() >= 6)
                    btnResetPassword.setEnabled(true);
                else
                    btnResetPassword.setEnabled(false);
            } else {
                pwConf.setError(getString(R.string.password_no_match));
                pwConf.setErrorTextAppearance(R.style.ErrorMessage);
                btnResetPassword.setEnabled(false);
            }


        }
    };

    /**
     * Redirects user to Main Activity on successful password reset.
     */
    public void onSuccessfulReset(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Redirects user to Login Activity if an error occurs during password reset.
     * @param message Error message to be displayed.
     */
    public void onErrorReset(String message){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(RESET_PASS_ERROR, message);
        startActivity(intent);
        finish();
    }

    /**
     * Resets user's password
     * @param view
     */
    public void resetPassword(View view) {
        resetPassProgressBar.setVisibility(View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager != null && getCurrentFocus() != null){
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        String password = passwordField.getText().toString();
        FirebaseAuth auth = FirebaseAuth.getInstance();

        System.out.println(deepLink[0].toString());
        if(user[0] != null) {
            auth.verifyPasswordResetCode(user[0]).addOnCompleteListener(new OnCompleteListener<String>() {
                @Override
                public void onComplete(@NonNull Task<String> task) {
                    auth.confirmPasswordReset(user[0], password);
                    resetPassProgressBar.setVisibility(View.INVISIBLE);
                    onSuccessfulReset();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onErrorReset("An error occurred when resetting password");
                    resetPassProgressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(ResetPassword.this, getString(R.string.error_reset), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
