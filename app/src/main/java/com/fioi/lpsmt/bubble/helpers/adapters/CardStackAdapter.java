package com.fioi.lpsmt.bubble.helpers.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.helpers.ColorGenerator;
import com.fioi.lpsmt.bubble.model.Post;
import com.makeramen.roundedimageview.RoundedImageView;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Recycler view adapter for card stack view.
 */
public class CardStackAdapter extends RecyclerView.Adapter<CardStackAdapter.ViewHolder>{

	private List<Post> posts;
	private Context ctx;

	public CardStackAdapter(Context ctx, List<Post> posts){
		this.posts = posts;
		this.ctx = ctx;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		return new ViewHolder(inflater.inflate(R.layout.card, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

		Post post = posts.get(position);

		final int SECOND = 1000;
		final int MINUTE = 60 * SECOND;
		final int HOUR = 60 * MINUTE;

		long ms = System.currentTimeMillis() - post.getCreatedAt();

		Uri imageUri = null;
		String ownerName = post.getOwnerInfo().get("name");
		holder.user.setText(ownerName);

		String image = post.getOwnerInfo().get("PhotoUrl");
		if(image != null){
			imageUri = Uri.parse(image);
			Glide.with(holder.userProfileImage)
					.load(imageUri)
					.signature(new ObjectKey(imageUri.hashCode()))
					.into(holder.userProfileImage);
		}

		holder.postBody.setText(post.getBody());
		holder.postTitle.setText(post.getTitle());
		holder.userProfileImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
		holder.userProfileImage.setCornerRadius(30);

		if (ms > HOUR) {
			holder.publishedAt.setText(String.format(ctx.getString(R.string.published), ms / HOUR));
		}else if (ms > MINUTE) {
			holder.publishedAt.setText(ctx.getString(R.string.less_than_one_hour));
		}else if (ms > SECOND) {
			holder.publishedAt.setText(ctx.getString(R.string.less_than_one_hour));
		}


		if(post.getUser() != null && ownerName != null && ownerName.startsWith("http"))
			Glide.with(holder.backgroundImage)
					.load(post.getUser())
					.into(holder.backgroundImage);
		else{
			holder.backgroundImage.setBackgroundColor(holder.generator.getRandomColor());
		}

	}

	@Override
	public int getItemCount() {
		return posts.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	class ViewHolder extends RecyclerView.ViewHolder{
		private TextView user, postTitle, postBody;
		private RoundedImageView backgroundImage, userProfileImage;
		private ColorGenerator generator = ColorGenerator.MATERIAL;
		private TextView publishedAt;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			user = itemView.findViewById(R.id.item_username);
			postTitle = itemView.findViewById(R.id.item_title);
			postBody = itemView.findViewById(R.id.item_body);
			backgroundImage = itemView.findViewById(R.id.item_image);
			userProfileImage = itemView.findViewById(R.id.user_image);
			publishedAt = itemView.findViewById(R.id.published_at);
		}
	}
}