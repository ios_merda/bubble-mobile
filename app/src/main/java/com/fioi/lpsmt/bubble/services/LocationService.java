package com.fioi.lpsmt.bubble.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final String LOCATION_SERVICE_TAG = "SERVICE_LIFECYCLE";

    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() + "LocationBroadcast";
    private static final String TAG = LocationService.class.getSimpleName();

    private FusedLocationProviderClient fusedLocationProviderClient;
    private GoogleApiClient locationClient;
    private SettingsClient settingsClient;
    private LocationCallback locationCallback;
    private Location location;
    private LocationRequest locationRequest;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final float LOCATION_UPDATE_SMALLEST_DISPLACEMENT = 1000;

    public LocationService() {
    }

    /**
     * Initializes location features such as update interval.
     */
    private void initLocationFeatures() {
        Log.d("LOCATION_FEATURES", "Initializing location features");

        locationRequest = new LocationRequest();
        locationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setSmallestDisplacement(LOCATION_UPDATE_SMALLEST_DISPLACEMENT);

        locationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                if(locationResult == null)
                    return;

                super.onLocationResult(locationResult);

                location = locationResult.getLastLocation();
                sendMessageToUI();


            }
        };

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationClient.connect();

    }


    /**
     * Executes when service is started
     * @param intent Intent to start the service.
     * @param flags
     * @param startId
     * @return
     */
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.d(LOCATION_SERVICE_TAG, "This service has started");
        initLocationFeatures();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOCATION_SERVICE_TAG, "This service has binded");
        return null;
    }

    /**
     * Activates when service is started and connected.
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(LOCATION_SERVICE_TAG, "This service is connected");
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "Error On onConnected().... Permission not granted");
            return;
        }

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    /**
     * Activates when service is suspended.
     * @param i
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection is suspended....");
    }

    /**
     * Activates on service error.
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed....");
    }

    /**
     * Handles communication with Main Activity.
     */
    private void sendMessageToUI() {
        Log.d("UI_GET_MESSAGE", "Sending info.....");
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra("LOCATION", location);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Executes when service is destroyed.
     */
    @Override
    public void onDestroy(){
        Log.d(LOCATION_SERVICE_TAG, "This service is destroyed");
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}
