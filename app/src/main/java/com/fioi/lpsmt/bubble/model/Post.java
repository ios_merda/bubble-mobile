package com.fioi.lpsmt.bubble.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Post implements Parcelable {
    private String id;
    private User owner;
    private String user;
    private String title;
    private String body;
    private int category;
    private long createdAt;
    private Date expiration;
    private Map<String, String> ownerInfo;

    public Post() {
    }

    public Post(String user, String title, String body, Date expiration) {
        this.user = user;
        this.title = title;
        this.body = body;
        this.expiration = expiration;
    }

    public Post(String id, String user, String title, String body, Date expiration) {
        this.setId(id);
        this.user = user;
        this.title = title;
        this.body = body;
        this.expiration = expiration;
    }

    public Post(Parcel in){
        this.id = in.readString();
        this.body = in.readString();
        this.title = in.readString();
        DateFormat format = new SimpleDateFormat("dd/mm/yyyy", Locale.ENGLISH);
        try {
            this.expiration = format.parse(in.readString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.category = Integer.parseInt(in.readString());
        this.user = in.readString();
        Map<String, String> info = new HashMap<>();
        info.put("name", in.readString());
        info.put("photoUrl", in.readString());
        this.ownerInfo = info;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(Map<String, String> ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.body);
        dest.writeString(this.title);
        if(this.expiration != null)
            dest.writeString(this.expiration.toString());
        dest.writeInt(this.category);
        dest.writeString(this.user);
        dest.writeString(this.ownerInfo.get("name"));
        dest.writeString(this.ownerInfo.get("PhotoUrl"));

    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>(){
        public Post createFromParcel(Parcel in){
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[0];
        }
    };

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}



