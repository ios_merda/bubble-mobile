package com.fioi.lpsmt.bubble;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.fioi.lpsmt.bubble.helpers.Validator;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class EditUserActivity extends AppCompatActivity {
    private boolean AVATAR_CHOSEN = false;
    public static final int REQUEST_GET_SINGLE_FILE = 1;
    private static final String TAG = EditUserActivity.class.getSimpleName();


    public EditText name;
    private String prevName;
    public TextInputEditText email;
    public TextInputLayout mail, pw, pwConf;
    public TextInputEditText editTextPassword;
    public TextInputEditText editTextConfirmPassword;
    public ImageView defaultImage;
    public Button update;
    private ProgressBar postProgressBar;
    private Uri newImage;
    private boolean edited = false;

    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(inputManager != null && getCurrentFocus() != null){
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            setResult(RESULT_CANCELED);
            finish();
        });

        update = findViewById(R.id.btnUpdate);
        update.setEnabled(false);

        defaultImage = findViewById(R.id.avatar);

        name = findViewById(R.id.txtName);
        name.addTextChangedListener(fieldsWatcher);

        email = findViewById(R.id.txtEmail);
        email.addTextChangedListener(fieldsWatcher);
        email.addTextChangedListener(emailWatcher);

        editTextPassword = findViewById(R.id.txtPassword);
        editTextPassword.addTextChangedListener(fieldsWatcher);
        editTextPassword.addTextChangedListener(passwStrengthTextEditorWatcher);
        editTextPassword.addTextChangedListener(passwMatchTextEditorWatcher);

        editTextConfirmPassword = findViewById(R.id.txtConfirmPassw);
        editTextConfirmPassword.addTextChangedListener(fieldsWatcher);
        editTextConfirmPassword.addTextChangedListener(passwMatchTextEditorWatcher);

        mail = findViewById(R.id.textMailInputLayout);
        pw = findViewById(R.id.textPasswordInputLayout);
        pwConf = findViewById(R.id.textPasswordConfInputLayout);


        postProgressBar = findViewById(R.id.newPostProgressBar);
        postProgressBar.setVisibility(View.GONE);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        try {
            postProgressBar.setVisibility(View.VISIBLE);
            loadCurrentUserData();
            postProgressBar.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Slidr.attach(this, configSlidr());
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder().listener(new SlidrListener() {
            @Override
            public void onSlideStateChanged(int state) {

            }

            @Override
            public void onSlideChange(float percent) {

            }

            @Override
            public void onSlideOpened() {

            }

            @Override
            public void onSlideClosed() {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(inputManager != null && getCurrentFocus() != null){
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                setResult(RESULT_CANCELED);
                finish();
            }
        }).build();
    }

    /**
     * Implicit intent for selecting image from gallery.
     * @param view
     */
    public void pickImage(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GET_SINGLE_FILE);
    }

    /**
     * Activates when user returns to edit user activity with a result from other activities.
     * @param requestCode Request code of operation.
     * @param resultCode Result code of operation.
     * @param data Data passed from other activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_GET_SINGLE_FILE) {
                    // probably can save this in user model instead of bitmap.
                    Uri selectedImageUri = data.getData();

                    if (selectedImageUri != null){
                        //u.setImage(selectedImageUri.toString());
                        AVATAR_CHOSEN = true;
                    }
                    // Get the path from the Uri
                    final String path = getPathFromURI(selectedImageUri);

                    if (path != null) {
                        File f = new File(path);
                        selectedImageUri = Uri.fromFile(f);
                    }
                    // Set the image in ImageView
                    ImageView avatar = (ImageView) findViewById(R.id.avatar);
                    avatar.setImageURI(selectedImageUri);
                    newImage = selectedImageUri;
                }
            }

        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    /**
     * Gets path as string from local uri of image.
     * @param contentUri Local uri of image
     * @return the path of the image.
     */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    /**
     * Loads the current user's data to be updated
     */
    public void loadCurrentUserData() {
        if (currentUser != null) {
            email.setText(currentUser.getEmail());
        }

        DocumentReference docRef = FirebaseFirestore.getInstance().collection("users").document(currentUser.getUid());

        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document != null && document.exists()) {
                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                    prevName = document.getString("Name");
                    name.setText(prevName);
                    String photoUrl = document.getString("PhotoUrl");
                    if(photoUrl != null){
                        newImage = Uri.parse(photoUrl);
                        new DownloadBitmapFromUri(EditUserActivity.this).execute(newImage);
                    }else{
                        defaultImage.setImageResource(R.drawable.user_add);
                    }
                }
            } else {
                Log.e(TAG, "get failed with ", task.getException());
            }
        });
    }

    /**
     * Async task for downloading user profile pic from Firebase URI.
     */
    private static class DownloadBitmapFromUri extends AsyncTask<Uri, Void, Bitmap> {

        private WeakReference<Context> activityReference;

        DownloadBitmapFromUri(Context context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Bitmap doInBackground(Uri... uris) {
            Bitmap bmp = null;
            try {
                URL ulrn = new URL(uris[0].toString());
                HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
                InputStream is = con.getInputStream();
                bmp = BitmapFactory.decodeStream(is);
                if (null != bmp)
                    return bmp;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            EditUserActivity context = (EditUserActivity) activityReference.get();
            BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap);
            context.defaultImage.setImageDrawable(bitmapDrawable);
        }
    }

    /**
     * Updates user data
     * @param view
     */
    public void updateUser(View view) {
        postProgressBar.setVisibility(View.VISIBLE);

        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager != null && getCurrentFocus() != null){
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        FirebaseStorage storage;
        StorageReference storageRef;

        storage = FirebaseStorage.getInstance("gs://bubble-1556982239730.appspot.com/");
        storageRef = storage.getReference();


        db.collection("users").document(currentUser.getUid())
                .update("Name", name.getText().toString(), "Email", email.getText().toString())
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "User name and/or email successfully updated");
                    edited = true;

                    if(AVATAR_CHOSEN){
                        Bitmap bitmap = ((BitmapDrawable) defaultImage.getDrawable()).getBitmap();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] data = baos.toByteArray();
                        StorageReference avatarRef = storageRef.child("usersAvatars/" + currentUser.getUid());
                        UploadTask uploadTask = avatarRef.putBytes(data);
                        uploadTask.addOnFailureListener(exception -> {
                            Toast.makeText(EditUserActivity.this,
                                    getString(R.string.error_update_name_photo), Toast.LENGTH_LONG)
                                    .show();
                        }).addOnSuccessListener(taskSnapshot -> {
                            avatarRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    db.collection("users").document(currentUser.getUid()).update("PhotoUrl", uri.toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "Image url stored successfully");
                                            onSuccessfulUpdate();
                                            postProgressBar.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }
                            });
                            Log.d(TAG, "Image successfully updated");
                        });
                    }else{
                        onSuccessfulUpdate();
                        postProgressBar.setVisibility(View.INVISIBLE);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error writing document", e);
                    postProgressBar.setVisibility(View.INVISIBLE);
                });

        if(!email.getText().toString().equals(currentUser.getEmail())) {
            currentUser.updateEmail(email.getText().toString())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User email address updated.");
                            edited = true;
                            postProgressBar.setVisibility(View.INVISIBLE);
                            onSuccessfulUpdate();
                        } else {
                            Toast.makeText(EditUserActivity.this,
                                    getString(R.string.error_update_email_password), Toast.LENGTH_LONG)
                                    .show();
                            postProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }


        if (!(editTextPassword.getText().toString().isEmpty())) {
            currentUser.updatePassword(editTextConfirmPassword.getText().toString())
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User password updated.");
                            edited = true;
                            postProgressBar.setVisibility(View.INVISIBLE);
                            onSuccessfulUpdate();
                        } else {
                            Toast.makeText(EditUserActivity.this,
                                    getString(R.string.error_update_email_password), Toast.LENGTH_LONG)
                                    .show();
                            postProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }

    /**
     * Activates upon successful update and redirects user to Main Activity.
     */
    private void onSuccessfulUpdate() {
        if(edited){
            setResult(RESULT_OK);
        }else{
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    private final TextWatcher fieldsWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            if(name.getText().toString().length()> 0
                    && email.getText().toString().length() > 0
                    && new Validator(Validator.EMAIL_PATTERN).validate(email.getText().toString())
                    && editTextConfirmPassword.getText().toString().equals(editTextPassword.getText().toString())){

                update.setEnabled(true);

            }else{

                update.setEnabled(false);

            }
        }
    };

    private final TextWatcher emailWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            if(!check) {
                update.setEnabled(false);
                mail.setError(getString(R.string.mail_not_valid));
                mail.setErrorTextAppearance(R.style.ErrorMessage);
            } else {
                mail.setError(getString(R.string.valid_mail));
                mail.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }

        public void afterTextChanged(Editable s) {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            if(!check) {
                update.setEnabled(false);
                mail.setError(getString(R.string.mail_not_valid));
                mail.setErrorTextAppearance(R.style.ErrorMessage);
            } else {
                mail.setError(getString(R.string.valid_mail));
                mail.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }
    };

    private final TextWatcher passwStrengthTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            pw.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s) {
            if (editTextPassword.getText().toString().isEmpty()) {
                pw.setError(getString(R.string.not_entered));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<6 && s.length()>0) {
                update.setEnabled(false);
                pw.setError(getString(R.string.easy));
                pw.setErrorTextAppearance(R.style.ErrorMessage);
            } else if(s.length()<10) {
                pw.setError(getString(R.string.medium));
                pw.setErrorTextAppearance(R.style.MediumPasswordMessage);
            } else {
                pw.setError(getString(R.string.strong));
                pw.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }
    };

    private final TextWatcher passwMatchTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            //pwConf.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s)
        {
            if (!(editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) ||
                    editTextPassword.getText().toString().isEmpty() || editTextConfirmPassword.getText().toString().isEmpty()) {
                pwConf.setError(getString(R.string.password_no_match));
                pwConf.setErrorTextAppearance(R.style.ErrorMessage);
            } else {
                pwConf.setError(getString(R.string.password_match));
                pwConf.setErrorTextAppearance(R.style.StrongPasswordMessage);
            }
        }
    };
}
