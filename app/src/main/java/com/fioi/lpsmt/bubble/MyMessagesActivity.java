package com.fioi.lpsmt.bubble;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.fioi.lpsmt.bubble.helpers.adapters.MyMessagesAdapter;
import com.fioi.lpsmt.bubble.model.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.r0adkll.slidr.Slidr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

public class MyMessagesActivity extends AppCompatActivity {
    private static final String TAG = "MY_MESSAGES_ACTIVITY";
    private RecyclerView listMyMessages;
    private ProgressBar myMessagesProgressBar;
    private MyMessagesAdapter adapter;
    private LinearLayoutManager manager;
    private List<Message> messages;
    private TextView clickToEnterChat;
    private TextView stillNoMessageHere;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mymessages);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> finish());
        messages = new ArrayList<>();

        listMyMessages = findViewById(R.id.mymessages_list);
        myMessagesProgressBar = findViewById(R.id.myMessagesProgressBar);
        myMessagesProgressBar.setVisibility(View.GONE);
        manager = new LinearLayoutManager(MyMessagesActivity.this);
        listMyMessages.setLayoutManager(manager);
        clickToEnterChat = findViewById(R.id.click_text);
        clickToEnterChat.setVisibility(View.GONE);
        stillNoMessageHere = findViewById(R.id.still_no_message_here);
        stillNoMessageHere.setVisibility(View.GONE);

        Slidr.attach(this);

        adapter = new MyMessagesAdapter(messages);
        listMyMessages.setAdapter(adapter);
        loadPosts();
    }

    /**
     * Gets the current user's conversations
     */
    private void loadPosts() {
        myMessagesProgressBar.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        db.collection("post_members").whereArrayContains("members", FirebaseAuth.getInstance().getCurrentUser().getUid()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Log.e(TAG, "Listener Failed", e);
                    return;
                }

                Set<String> posts = new HashSet<>();
                if(queryDocumentSnapshots.isEmpty()) {
                    stillNoMessageHere.setVisibility(View.VISIBLE);
                } else {
                    clickToEnterChat.setVisibility(View.VISIBLE);
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        posts.add(documentSnapshot.getId());
                    }

                    for (String postId : posts) {
                        db.collection("messages")
                                .whereEqualTo("post", postId)
                                .orderBy("sent", Query.Direction.DESCENDING)
                                .limit(1)
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            Log.e(TAG, "Listener Failed", e);
                                            return;
                                        }

                                        assert queryDocumentSnapshots != null;
                                        List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                                        if(documentSnapshots.size()>0){
                                            Message message = new Message(
                                                    documentSnapshots.get(0).getString("postName"),
                                                    documentSnapshots.get(0).getString("senderName"),
                                                    documentSnapshots.get(0).getString("text"),
                                                    documentSnapshots.get(0).getString("post")
                                            );
                                            message.setSent(documentSnapshots.get(0).getLong("sent"));
                                            if(!messages.isEmpty()) {
                                                int posToRemove=-1;
                                                for (Message m : messages) {
                                                    if (m.getPost().equals(message.getPost())) {
                                                        posToRemove = messages.indexOf(m);
                                                    }
                                                }
                                                if(posToRemove != -1) {
                                                    messages.remove(posToRemove);
                                                    messages.add(0, message);
                                                } else {
                                                    messages.add(0, message);
                                                }
                                                Collections.sort(messages, new Comparator<Message>() {
                                                    @Override
                                                    public int compare(Message o1, Message o2) {
                                                        if(o1.getSent() == o2.getSent())
                                                            return 0;
                                                        return o1.getSent() < o2.getSent()? 1 : -1;
                                                    }
                                                });
                                            } else {
                                                messages.add(0, message);

                                            }
                                            adapter.notifyDataSetChanged();
                                        }

                                    }
                        });

                    }
                }
                myMessagesProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }
}
