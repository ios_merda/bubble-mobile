package com.fioi.lpsmt.bubble;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.TaskStackBuilder;

public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {


    public static final String DARK_MODE_SWITCH = "dark_mode_switch";
    public static final String EDIT_INFO = "edit_info";
    public static boolean edited = false;

    /**
     * Sets up the action bar for the activity.
     *
     * @param savedInstanceState Saved instance state bundle.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            if(edited)
                setResult(RESULT_OK);
            else
                setResult(RESULT_CANCELED);
            finish();
        });
        Slidr.attach(this, configSlidr());
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        if(edited)
                            setResult(RESULT_OK);
                        else
                            setResult(RESULT_CANCELED);
                        finish();
                    }
                }).build();
    }


    /**
     * Handles transition to Dark Theme.
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(DARK_MODE_SWITCH)){
            Boolean darkMode = sharedPreferences.getBoolean(DARK_MODE_SWITCH, false);
            changeTheme(darkMode);
        }
    }

    /**
     * Changes the app theme
     * @param darkMode
     */
    public void changeTheme(Boolean darkMode){
        AppCompatDelegate.setDefaultNightMode(darkMode ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
        TaskStackBuilder.create(this)
                .addNextIntent(new Intent(this, MainActivity.class))
                .addNextIntent(this.getIntent())
                .startActivities();
    }

    @Override
    public void onBackPressed() {
        if (edited) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }




}
