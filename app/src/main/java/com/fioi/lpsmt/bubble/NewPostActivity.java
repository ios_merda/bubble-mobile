package com.fioi.lpsmt.bubble;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.GeoPoint;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import org.imperiumlabs.geofirestore.GeoFirestore;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class NewPostActivity extends AppCompatActivity {
    private static final String TAG = "NEW_POST_ACTIVITY";
    private Location location;
    private TextView locationTv;
    private EditText postTitle;
    private EditText postDesc;
    private ProgressBar postProgressBar;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private GeoFirestore geoFirestore;

    private UserPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(inputManager != null && getCurrentFocus() != null){
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            location = (Location)extras.get("LOCATION");
            locationTv = findViewById(R.id.txtLocation);
            locationTv.setText(getCityName(location));
        }
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        CollectionReference geoFirestoreRef = FirebaseFirestore.getInstance().collection("posts");
        geoFirestore = new GeoFirestore(geoFirestoreRef);

        postProgressBar = findViewById(R.id.newPostProgressBar);
        postProgressBar.setVisibility(View.GONE);

        findViewById(R.id.txtLocation).setEnabled(false);
        findViewById(R.id.btnPublish).setEnabled(false);

        postTitle = (EditText) findViewById(R.id.txtPostTitle);
        postTitle.addTextChangedListener(titleAndBodyWatcher);

        postDesc = (EditText) findViewById(R.id.txtPostBody);
        postDesc.addTextChangedListener(titleAndBodyWatcher);
        userPreferences = new UserPreferences(this);

        Slidr.attach(this, configSlidr());
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if(inputManager != null && getCurrentFocus() != null){
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }).build();
    }

    private final TextWatcher titleAndBodyWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            if(postTitle.getText().toString().length() == 0 || postDesc.getText().toString().length() == 0) {
                findViewById(R.id.btnPublish).setEnabled(false);
            } else {
                findViewById(R.id.btnPublish).setEnabled(true);
            }
        }
    };


    /**
     * Gets the name of the city closest to the current location
     * @param location current location
     * @return String instance.
     */
    private String getCityName(Location location) {
        String cityName = null;
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());

        List<Address>  addresses;
        if(location != null){
            try {
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0)
                    System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
                return cityName;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "Location Unavailable";
    }

    /**
     * Gets post data from post creation fields and prepares model to save to Firebase.
     * @param view
     */
    public void savePost(View view) {
        postProgressBar.setVisibility(View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager != null && getCurrentFocus() != null){
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        EditText postTitle = findViewById(R.id.txtPostTitle);
        EditText postBody = findViewById(R.id.txtPostBody);
        Spinner postLifetime = findViewById(R.id.spinLifeTime);
        Spinner postCategory = findViewById(R.id.spinCategory);

        String[] lifeTime = postLifetime.getSelectedItem().toString().split("\\s+");
        Date exp = getExpirationDate(lifeTime[0]);

        if(location == null){
            Toast.makeText(this, getString(R.string.location_not_available), Toast.LENGTH_LONG).show();

        }else{
            FirebaseUser user = mAuth.getCurrentUser();
            Post p = new Post(user.getUid(), postTitle.getText().toString(), postBody.getText().toString(), exp);
            Map<String, String> ownerInfo = new HashMap<>();
            ownerInfo.put("name", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_NAME));
            ownerInfo.put("PhotoUrl", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_USER_IMAGE));
            p.setOwnerInfo(ownerInfo);
            p.setCategory(postCategory.getSelectedItemPosition());
            p.setCreatedAt(System.currentTimeMillis());
            insertPost(p);
        }

    }

    /**
     * Handles saving post data to Firebase.
     * @param p the post to be saved.
     */
    private void insertPost(Post p) {
        db.collection("posts")
                .add(p)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        setGeoLocation(documentReference.getId());
                        Log.d(TAG, "Post created successfully.");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("addPost", "Error writing document", e);
                    }
                });
    }

    /**
     * Sets post's location and adds the current user to the associated chatroom.
     * @param id the id of the just inserted post
     */
    private void setGeoLocation(String id){
        geoFirestore.setLocation(id, new GeoPoint(location.getLatitude(), location.getLongitude()), new GeoFirestore.CompletionListener() {
            @Override
            public void onComplete(Exception exception) {
                if (exception == null){
                    Log.d("LOCATION_PROBLEM", "Location set successfully");
                    List<String> userIds = new ArrayList<>();
                    userIds.add(mAuth.getCurrentUser().getUid());
                    Map<String, Object> data = new HashMap<>();
                    data.put("members", userIds);
                    db.collection("post_members").document(id).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "Successfully added new member to chat room");
                            db.collection("post_members")
                                    .document(id)
                                    .update("members", FieldValue.arrayUnion(mAuth.getCurrentUser().getUid()))
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "Successfully added new member to chat room");
                                            postProgressBar.setVisibility(View.INVISIBLE);
                                            setResult(RESULT_OK);
                                            finish();

                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.e(TAG, "Failed to add new member to chat room", e);
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to add new member to chat room", e);
                        }
                    });
                }else{
                    Log.e("LOCATION_PROBLEM", "Could not set location", exception);
                }
            }
        });
    }

    /**
     * Sets the post's expiration date
     * @param lifeTime the post's time to live
     */
    private Date getExpirationDate(String lifeTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar postExp = Calendar.getInstance();
        postExp.setTime(date);
        postExp.add(Calendar.HOUR, + Integer.parseInt(lifeTime));
        return postExp.getTime();
    }

}
