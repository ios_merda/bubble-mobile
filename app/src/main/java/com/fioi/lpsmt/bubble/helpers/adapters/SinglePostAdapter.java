package com.fioi.lpsmt.bubble.helpers.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.model.Post;
import java.text.SimpleDateFormat;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * RecyclerView adapter for user's personal posts view.
 */
public class SinglePostAdapter extends RecyclerView.Adapter<SinglePostAdapter.ViewHolder> {

    private List<Post> posts;

    public SinglePostAdapter(List<Post> posts) {
        this.posts = posts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.singlepostitem, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Post p = posts.get(position);
        holder.body.setText(p.getBody());
        holder.title.setText(p.getTitle());
        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");
        holder.expiration.setText(dt1.format(p.getExpiration()));
    }

    public void removeItem(int position) {
        posts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, posts.size());
    }
    public void restoreItem(Post post, int position) {
        posts.add(position, post);
        notifyItemInserted(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView title, body, expiration;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.myposts_title);
            body = itemView.findViewById(R.id.myposts_body);
            expiration = itemView.findViewById(R.id.myposts_date);
        }


    }
}
