package com.fioi.lpsmt.bubble.model;

import android.net.Uri;

import java.io.Serializable;

public class User implements Serializable {
    private String userId;
    private String Name;
    private String Email;
    private Uri PhotoUrl;

    public User(){}

    public User(String userId, String name, String email, Uri imageUri){
        this.setUserId(userId);
        this.setName(name);
        this.setEmail(email);
        this.setImageUri(imageUri);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public Uri getImageUri() {
        return PhotoUrl;
    }

    public void setImageUri(Uri imageUri) {
        this.PhotoUrl = imageUri;
    }
}
