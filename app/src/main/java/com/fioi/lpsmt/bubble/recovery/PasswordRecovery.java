package com.fioi.lpsmt.bubble.recovery;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.helpers.Validator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.FirebaseAuth;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;


public class PasswordRecovery extends AppCompatActivity {
    private EditText emailField;
    private TextInputLayout mail;
    private Button btnPassRecoverySend;
    private ProgressBar passRecoveryProgressBar;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    UserPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(inputManager != null && getCurrentFocus() != null){
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            finish();
        });

        emailField = findViewById(R.id.passRecoveryEmail);
        emailField.addTextChangedListener(emailWatcher);
        mail = findViewById(R.id.textPassRecoveryEmail);
        btnPassRecoverySend = findViewById(R.id.passRecoverySend);
        btnPassRecoverySend.setEnabled(false);
        passRecoveryProgressBar = findViewById(R.id.passRecoveryProgressBar);
        passRecoveryProgressBar.setVisibility(View.GONE);

        userPreferences = new UserPreferences(this);

        Slidr.attach(this, configSlidr());
    }

    /**
     * Sends password reset link by email
     * @param view
     */
    public void sendResetPassLink(View view) {
        String email = emailField.getText().toString();
        passRecoveryProgressBar.setVisibility(View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager != null && getCurrentFocus() != null){
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        ActionCodeSettings actionCodeSettings = ActionCodeSettings.newBuilder()
                .setUrl("https://bubble.alessandrogerevini.com/resetpassword")
                .setHandleCodeInApp(true)
                .setAndroidPackageName("com.fioi.lpsmt.bubble", false, null)
                .build();

        auth.sendPasswordResetEmail(email, actionCodeSettings)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("reset:", "Email sent.");
                            //userPreferences.putString(UserPreferences.SHARED_PREFERENCES_LOCAL_EMAIL, email);
                            Toast toast = Toast.makeText(PasswordRecovery.this, getString(R.string.chack_email_for_pw_reset), Toast.LENGTH_LONG);
                            passRecoveryProgressBar.setVisibility(View.INVISIBLE);
                            toast.show();
                        } else {
                            Exception e = task.getException();
                            Log.w("reseterror", "passwordResetRequest:failure " + e.getMessage(), task.getException());
                            Toast toast = Toast.makeText(PasswordRecovery.this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
                            passRecoveryProgressBar.setVisibility(View.INVISIBLE);
                            toast.show();
                        }
                    }
                });
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if(inputManager != null && getCurrentFocus() != null){
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        finish();
                    }
                }).build();
    }

    private final TextWatcher emailWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mail.setError(getString(R.string.not_entered));
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            mail.setError(!check ? getString(R.string.mail_not_valid) : null);
            if(check)
                btnPassRecoverySend.setEnabled(true);
            else
                btnPassRecoverySend.setEnabled(false);
        }

        public void afterTextChanged(Editable s) {
            boolean check = new Validator(Validator.EMAIL_PATTERN).validate(s.toString());
            mail.setError(!check ? getString(R.string.mail_not_valid) : null);
            if(check)
                btnPassRecoverySend.setEnabled(true);
            else
                btnPassRecoverySend.setEnabled(false);
        }
    };
}
