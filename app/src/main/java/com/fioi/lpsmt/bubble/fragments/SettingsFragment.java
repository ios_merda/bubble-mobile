package com.fioi.lpsmt.bubble.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.EditUserActivity;
import com.fioi.lpsmt.bubble.MainActivity;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.SettingsActivity;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.TaskStackBuilder;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import static android.app.Activity.RESULT_OK;

/**
 * Fragment for displaying settings fields and data
 */
public class SettingsFragment extends PreferenceFragmentCompat {
	public static final int EDIT_REQUEST_CODE = 5;

	public SettingsFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.preferences, rootKey);
		Preference edit = findPreference(SettingsActivity.EDIT_INFO);
		String auth_type = new UserPreferences(getActivity().getApplicationContext()).getString(UserPreferences.USER_AUTH_TYPE);
		if(!(auth_type.equals("facebook.com")) && !(auth_type.equals("google.com")) && edit != null){
			edit.setEnabled(true);
			edit.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent edit = new Intent(getActivity(), EditUserActivity.class);
					startActivityForResult(edit, EDIT_REQUEST_CODE );
					return true;
				}
			});
		} else {
			edit.setEnabled(false);
			Toast.makeText(getActivity().getApplicationContext(), getString(R.string.no_edit_social_account), Toast.LENGTH_LONG).show();
		}
		findPreference(SettingsActivity.DARK_MODE_SWITCH).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Boolean darkMode = (Boolean) newValue;
				AppCompatDelegate.setDefaultNightMode(darkMode ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
				TaskStackBuilder.create(getActivity().getApplicationContext())
						.addNextIntent(new Intent(getActivity().getApplicationContext(), MainActivity.class))
						.addNextIntent(new Intent(getActivity().getApplicationContext(), SettingsActivity.class))
						.startActivities();
				getActivity().finish();
				return true;
			}
		});
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == SettingsFragment.EDIT_REQUEST_CODE){
			if(resultCode == RESULT_OK){
				SettingsActivity.edited = true;
			}
		}
	}

}