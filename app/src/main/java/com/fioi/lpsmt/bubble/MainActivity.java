package com.fioi.lpsmt.bubble;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.fioi.lpsmt.bubble.helpers.PostDeleteListener;
import com.fioi.lpsmt.bubble.helpers.adapters.CardStackAdapter;
import com.fioi.lpsmt.bubble.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.dialog.*;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.fioi.lpsmt.bubble.helpers.LocationCallback;
import com.fioi.lpsmt.bubble.helpers.SocialDisconnectHelper;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.services.LocationService;
import com.fioi.lpsmt.bubble.services.broadcastreceivers.GpsReceiver;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.GeoPoint;
import org.imperiumlabs.geofirestore.GeoFirestore;
import org.imperiumlabs.geofirestore.GeoQuery;
import org.imperiumlabs.geofirestore.callbacks.GeoQueryDataEventListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.yuyakaido.android.cardstackview.*;
import android.view.animation.LinearInterpolator;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CardStackListener {
    public GoogleSignInClient googleSignInClient;

    public static PostDeleteListener postDeleteListener;
    private PropertyChangeListener propertyChangeListener;

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int MY_POSTS_REQUEST_CODE = 1;
    private static final int NEW_POST_REQUEST_CODE = 2;
    private static final int POST_CHAT_REQUEST_CODE = 3;
    private static final int EDIT_USER_REQUEST_CODE = 4;

    private TextView displayName;
    private TextView displayEmail;
    private TextView txtLoadingLocation;
    private TextView txtLoadingPosts;
    private TextView txtNoPostFound;
    private ImageView displayProfilePic;
    private UserPreferences userPreferences;
    private FloatingActionButton fabRadius;
    private ProgressBar mainProgressBar;

    private Location location;
    private boolean requiredLocationSettingsSet = false;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GeoFirestore geoFirestore;
    private FirebaseUser user;
    private CollectionReference postReference;

    private boolean mAlreadyStartedService = false;
    private boolean isReportCheckBoxChecked;
    private String chosenCategory;
    private GpsReceiver gpsReceiver;

    private CardStackView cardStackView;
    private CardStackLayoutManager manager;
    private CardStackAdapter adapter;
    GuideView.Builder guideview = new GuideView.Builder(this);
    private ArrayList<Post> posts;

    private int currentCardPosition;
    private Direction lastSwipedDirection;
    private int checkBoxCounter = 0;
    private FloatingActionButton rewind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // PreferenceSettings
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean switchPref = sharedPref.getBoolean(SettingsActivity.DARK_MODE_SWITCH, false);
        if(switchPref){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rewind = findViewById(R.id.rewind_button);

        rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RewindAnimationSetting setting;
                if(lastSwipedDirection == Direction.Left){
                    setting = new RewindAnimationSetting.Builder()
                            .setDirection(Direction.Left)
                            .setDuration(Duration.Slow.duration)
                            .setInterpolator(new DecelerateInterpolator())
                            .build();
                }else{
                    setting = new RewindAnimationSetting.Builder()
                            .setDirection(Direction.Right)
                            .setDuration(Duration.Slow.duration)
                            .setInterpolator(new DecelerateInterpolator())
                            .build();
                }

                manager.setRewindAnimationSetting(setting);
                cardStackView.rewind();
            }
        });


        fabRadius = findViewById(R.id.radius_button);
        if(location == null){
            fabRadius.setEnabled(false);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        postReference = db.collection("posts");
        geoFirestore = new GeoFirestore(postReference);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Log.i("ON_CREATE_MAIN", "this returns in on create method as suspected");
        View headerView = navigationView.getHeaderView(0);

        displayEmail = headerView.findViewById(R.id.txtUserMailDialog);
        displayName = headerView.findViewById(R.id.txtUserNameDialog);
        displayProfilePic = headerView.findViewById(R.id.imageViewUser);
        txtLoadingLocation = findViewById(R.id.loading_location);
        txtLoadingLocation.setVisibility(View.GONE);
        txtLoadingPosts = findViewById(R.id.loading_posts);
        txtLoadingPosts.setVisibility(View.GONE);
        txtNoPostFound = findViewById(R.id.no_post_found);
        txtNoPostFound.setVisibility(View.GONE);
        mainProgressBar = findViewById(R.id.mainProgressBar);
        mainProgressBar.setVisibility(View.GONE);
        userPreferences = new UserPreferences(this);


        postDeleteListener = new PostDeleteListener();
        postDeleteListener.setValue("InitialValue");
        propertyChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                reloadPosts();
            }
        };
        postDeleteListener.addPropertyChangeListener(propertyChangeListener);

        cardStackView = findViewById(R.id.card_stack_view);
        manager = new CardStackLayoutManager(getApplicationContext(), this);
        posts = new ArrayList<>();

        SwipeRefreshLayout pullToRefresh = findViewById(R.id.pull_to_refresh_posts);
        pullToRefresh.setOnRefreshListener(() -> {
            chosenCategory = null;
            reloadPosts(); // your code
            pullToRefresh.setRefreshing(false);
        });
        if(userPreferences.getBoolean(UserPreferences.SHARED_PREFERENCES_FIRST_START))
            showIntro(getString(R.string.explore_posts_title), getString(R.string.explore_posts), R.id.card_stack_view, 1);
    }

    /**
     * Introduces user to the application features.
     * @param title the title of the feature
     * @param text how the feature works
     * @param viewId how to trigger the feature
     * @param type the feature id (used to give an order according to which to present the features)
     */
    private void showIntro(String title, String text, int viewId, final int type) {
        GuideView.Builder guideview = new GuideView.Builder(this)
                .setTitle(title)
                .setContentText(text)
                .setTargetView(findViewById(viewId))
                .setContentTextSize(12)
                .setTitleTextSize(14)
                .setDismissType(DismissType.anywhere);
        if(type == 1) {
            guideview.setIndicatorHeight((float) 20)
                    .setGravity(Gravity.center)
                    .setGuideListener(view -> showIntro(getString(R.string.reply_to_post_title), getString(R.string.reply_to_post), R.id.card_stack_view, 6))
                    .build()
                    .show();
        } else if (type == 6) {
            guideview.setIndicatorHeight((float) 20)
                    .setGravity(Gravity.center)
                    .setGuideListener(view -> showIntro(getString(R.string.filter), getString(R.string.press_to_filter), R.id.button_filter, 2))
                    .build()
                    .show();
        } else {
            guideview.setGuideListener(view -> {
                if (type == 2) {
                    showIntro(getString(R.string.title_activity_new_post), getString(R.string.press_to_create_new_post), R.id.add_button, 3);
                } else if (type == 4) {
                    showIntro(getString(R.string.txt_radius), getString(R.string.change_radius), R.id.radius_button, 5);
                } else if (type == 3) {
                    showIntro(getString(R.string.rewind), getString(R.string.review_post), R.id.rewind_button, 4);
                } else if (type == 5) {
                    userPreferences.putBoolean(UserPreferences.SHARED_PREFERENCES_FIRST_START, false);
                }
            }).build().show();
        }
    }

    /**
     * Sets up card settings.
     */
    private void setupCards() {
        manager.setStackFrom(StackFrom.Bottom);
        manager.setVisibleCount(3);
        manager.setTranslationInterval(8.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.3f);
        manager.setMaxDegree(20.0f);
        manager.setDirections(Direction.HORIZONTAL);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(false);
        manager.setOverlayInterpolator(new LinearInterpolator());

    }

    protected void onStart() {
        super.onStart();
        chosenCategory = null;
        if (location == null) {
            mainProgressBar.setVisibility(View.VISIBLE);
            txtLoadingLocation.setVisibility(View.VISIBLE);
        }
        user = mAuth.getCurrentUser();
        if (user != null) {
            loadUserData();

            if(userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS) == null){
                userPreferences.putString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS, Integer.toString(25));
            }

            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    location = intent.getParcelableExtra("LOCATION");
                    Log.d("LOCATION_RECEIVED", "lat:" + location.getLatitude() + " long:" + location.getLongitude());
                    fabRadius.setEnabled(true);
                    mainProgressBar.setVisibility(View.VISIBLE);
                    txtLoadingLocation.setVisibility(View.INVISIBLE);
                    txtLoadingPosts.setText(getString(R.string.loading_posts).concat(" " + userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS) + " km..."));
                    txtLoadingPosts.setVisibility(View.VISIBLE);
                    getAllPosts();
                }
            }, new IntentFilter(LocationService.ACTION_LOCATION_BROADCAST));

            gpsReceiver = new GpsReceiver(new LocationCallback() {
                @Override
                public void onLocationTriggered() {
                    Toast.makeText(MainActivity.this, getString(R.string.location_information_rationale), Toast.LENGTH_LONG).show();
                    mainProgressBar.setVisibility(View.INVISIBLE);
                    txtLoadingLocation.setVisibility(View.INVISIBLE);
                }
            });

            registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

            if(isGooglePlayServicesAvailable()){
                if(requiredLocationSettingsSet && checkPermissions()){
                    startLocationService();
                }else{
                    requestPermissions();
                }
            }else{
                Toast.makeText(getApplicationContext(), getString(R.string.install_gplay_services), Toast.LENGTH_LONG).show();
            }

        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Checks wheather Google Play Services is available on the user's phone.
     * @return true if installed.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Starts service for determining user location.
     */
    private void startLocationService(){
        if(!mAlreadyStartedService){
            Intent intent = new Intent(this, LocationService.class);
            startService(intent);
            mAlreadyStartedService = true;
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, LocationService.class));
        mAlreadyStartedService = false;
        if(gpsReceiver != null){
            unregisterReceiver(gpsReceiver);
            gpsReceiver = null;
        }

        postDeleteListener.removePropertyChangeListener(propertyChangeListener);
        super.onDestroy();
    }

    /**
     * Checks whether user has given permission to app to track location.
     * @return
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Creates a snackbar
     * @param mainTextStringId Text string id from resources.
     * @param actionStringId Action string id from resources.
     * @param listener Listener for determining action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Method for requesting location permissions and displays a snackbar with a rationale message
     * if user declines.
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);


        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    /**
     * Activates after user selects whether to give location access to app or not.
     * @param requestCode the request code for requesting permissions.
     * @param permissions string containing all permissions given to app by user.
     * @param grantResults results of permission requests.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission granted, updates requested, starting location updates");
                requiredLocationSettingsSet = true;
                startLocationService();
            } else {
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    /**
     * Activates when user returns to main activity with a result from other activities.
     * @param requestCode Request code of operation.
     * @param resultCode Result code of operation.
     * @param data Data passed from other activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        requiredLocationSettingsSet = true;
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        requiredLocationSettingsSet = false;
                        break;
                }
                break;
            case MY_POSTS_REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    Toast.makeText(this, "Deleting requested posts...", Toast.LENGTH_SHORT).show();
                    reloadPosts();
                }
                break;
            case NEW_POST_REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    reloadPosts();
                }
                break;
            case POST_CHAT_REQUEST_CODE:
                if(resultCode == RESULT_CANCELED){
                    RewindAnimationSetting setting;
                    if(lastSwipedDirection == Direction.Left){
                        setting = new RewindAnimationSetting.Builder()
                                .setDirection(Direction.Left)
                                .setDuration(Duration.Slow.duration)
                                .setInterpolator(new DecelerateInterpolator())
                                .build();
                    }else{
                        setting = new RewindAnimationSetting.Builder()
                                .setDirection(Direction.Right)
                                .setDuration(Duration.Slow.duration)
                                .setInterpolator(new DecelerateInterpolator())
                                .build();
                    }

                    manager.setRewindAnimationSetting(setting);
                    cardStackView.rewind();
                }
                break;
            case EDIT_USER_REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    loadUserData();
                }
                break;
        }
    }

    /**
     * Loads and sets the data for a registered user.
     */
    private void loadUserData(){
        db.collection("users").document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if(document != null && document.exists()){
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        String userName = document.getString("Name");
                        String email = document.getString("Email");
                        String photoUrl = document.getString("PhotoUrl");
                        displayName.setText(userName);
                        displayEmail.setText(email);
                        userPreferences.putString(UserPreferences.SHARED_PREFERENCES_USER_NAME, userName);
                        if(photoUrl != null){
                            new DownloadBitmapFromUri(MainActivity.this).execute(Uri.parse(photoUrl));
                            userPreferences.putString(UserPreferences.SHARED_PREFERENCES_USER_IMAGE, photoUrl);
                        }
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    /**
     * Disconnects from social media if user is logged in from a social account and clears User Preferences.
     * @param type the type of authentication (facebook, google, normal)
     */
    private void logout(String type) {
        switch (type) {
            case "facebook.com":
                SocialDisconnectHelper.disconnectFromFacebook(userPreferences);
                userPreferences.clearOnLogout();
                break;
            case "google.com":
                SocialDisconnectHelper.disconnectFromGoogle(googleSignInClient);
                userPreferences.clearOnLogout();
                break;
            default:
                userPreferences.clearOnLogout();
                break;
        }
        handleSessionExpired();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_posts) {
            Intent intent = new Intent(this, MyPostsActivity.class);
            startActivityForResult(intent, MY_POSTS_REQUEST_CODE);
        } else if (id == R.id.nav_messages) {
            startActivity(new Intent(this, MyMessagesActivity.class));
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, EDIT_USER_REQUEST_CODE);
        } else if (id == R.id.nav_logout) {
            db.collection("users").document(user.getUid()).update("deviceToken", "").addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "Device token deleted successfully");
                    mAuth.signOut();
                    logout(userPreferences.getString(UserPreferences.USER_AUTH_TYPE));
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "Could not delete device token.", e);
                }
            });

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Handles session exipiration and redirects user to login activity.
     */
    private void handleSessionExpired() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Loads all posts from Firebase in a specific area based on user position.
     */
    private void getAllPosts() {
        posts.clear();
        GeoQuery geoQuery = geoFirestore.queryAtLocation(new GeoPoint(location.getLatitude(), location.getLongitude()), Integer.parseInt(userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS)));
        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {
            @Override
            public void onDocumentEntered(@NonNull DocumentSnapshot documentSnapshot, @NonNull GeoPoint geoPoint) {
                Log.d("DATA_ARRIVED", "Data: " + documentSnapshot.getData());
                Timestamp expiration = documentSnapshot.getTimestamp("expiration");
                if(expiration != null){
                    if(expiration.compareTo(new Timestamp(new Date())) >= 0){
                        if(chosenCategory == null) {
                            Log.d("DATA_ARRIVED", "Data: " + documentSnapshot.getData());
                            String id = documentSnapshot.getId();
                            String title = (String) documentSnapshot.get("title");
                            String body = (String) documentSnapshot.get("body");
                            String ownerName = (String) documentSnapshot.get("ownerInfo.name");
                            String ownerImageStr = (String) documentSnapshot.get("ownerInfo.PhotoUrl");
                            long createdAt = documentSnapshot.getLong("createdAt");
                            Map<String, String> ownerInfo = new HashMap<>();
                            ownerInfo.put("name", ownerName);
                            ownerInfo.put("PhotoUrl", ownerImageStr);
                            Post post = new Post(id, null, title, body, null);
                            post.setOwnerInfo(ownerInfo);
                            post.setCreatedAt(createdAt);
                            posts.add(post);
                        } else {
                            if(documentSnapshot.get("category").toString().equals(chosenCategory)) {
                                Log.d("DATA_ARRIVED", "Data: " + documentSnapshot.getData());
        
                                String id = documentSnapshot.getId();
                                String title = (String) documentSnapshot.get("title");
                                String body = (String) documentSnapshot.get("body");
                                String ownerName = (String) documentSnapshot.get("ownerInfo.name");
                                String ownerImageStr = (String) documentSnapshot.get("ownerInfo.PhotoUrl");
                                long createdAt = documentSnapshot.getLong("createdAt");
                                Map<String, String> ownerInfo = new HashMap<>();
                                ownerInfo.put("name", ownerName);
                                ownerInfo.put("PhotoUrl", ownerImageStr);
                                Post post = new Post(id, null, title, body, null);
                                post.setOwnerInfo(ownerInfo);
                                post.setCreatedAt(createdAt);
                                posts.add(post);
                            }
                        }
                    }
                }

            }

            @Override
            public void onDocumentExited(@NonNull DocumentSnapshot documentSnapshot) {

            }

            @Override
            public void onDocumentMoved(@NonNull DocumentSnapshot documentSnapshot, @NonNull GeoPoint geoPoint) {

            }

            @Override
            public void onDocumentChanged(@NonNull DocumentSnapshot documentSnapshot, @NonNull GeoPoint geoPoint) {

            }

            @Override
            public void onGeoQueryReady() {
                mainProgressBar.setVisibility(View.INVISIBLE);
                txtLoadingPosts.setVisibility(View.INVISIBLE);
                if(posts.size() == 0){
                    txtNoPostFound.setVisibility(View.VISIBLE);
                }else{
                    txtNoPostFound.setVisibility(View.INVISIBLE);
                    adapter = new CardStackAdapter(MainActivity.this, posts);
                    cardStackView.setAdapter(adapter);
                    setupCards();
                    cardStackView.setLayoutManager(manager);
                }

            }

            @Override
            public void onGeoQueryError(@NonNull Exception e) {

            }
        });

    }

    /**
     * Redirects user to the NewPostActivity in order to create a new post.
     * @param view
     */
    public void goToNewPost(View view) {
        Intent intent = new Intent(this, NewPostActivity.class);
        intent.putExtra("LOCATION", location);
        startActivityForResult(intent, NEW_POST_REQUEST_CODE);
    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card is dragged.
     * @param direction the direction in which the card is dragged.
     * @param ratio
     */
    @Override
    public void onCardDragging(Direction direction, float ratio) {
        
    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card is swiped.
     * @param direction the direction in which the card is swiped.
     */
    @Override
    public void onCardSwiped(Direction direction) {
        lastSwipedDirection = direction;
        if(direction.equals(Direction.Left)){
            String postId = posts.get(currentCardPosition).getId();
            String postTitle = posts.get(currentCardPosition).getTitle();
            openPostChat(postId, postTitle);
        }

    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card is rewound.
     */
    @Override
    public void onCardRewound() {
    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card is cancelled.
     */
    @Override
    public void onCardCanceled() {

    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card appears.
     * @param view
     * @param position
     */
    @Override
    public void onCardAppeared(View view, int position) {
        currentCardPosition = position;
    }

    /**
     * Overridden method from CardStackListenerInterface. Activates when a card disappears.
     * @param view
     * @param position
     */
    @Override
    public void onCardDisappeared(View view, int position) {
    }

    /**
     * Handles the radius button and sets the radius based on user input.
     * @param view
     */
    public void openRadiusDialog(View view) {
        LinearLayout dialog = new LinearLayout(this);
        dialog.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogParams.setMargins(16,16,16,16);
        dialog.setLayoutParams(dialogParams);
        dialog.setPadding(16,16,16,16);
        SeekBar seekBar = new SeekBar(this);
        seekBar.setMax(50);
        seekBar.setProgress(Integer.parseInt(userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS)));
        seekBar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorSecondary, typedValue, true);
        seekBar.getProgressDrawable().setColorFilter(typedValue.data, PorterDuff.Mode.SRC_ATOP);
        seekBar.getThumb().setColorFilter(typedValue.data, PorterDuff.Mode.SRC_ATOP);
        TextView textView = new TextView(this);
        textView.setText(getString(R.string.txt_radius) + ": " + userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS) + " km");
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.setMargins(32,16,16,16);
        textView.setLayoutParams(textParams);
        dialog.addView(seekBar);
        dialog.addView(textView);
        MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.choose_radius))
                .setView(dialog)
                .setPositiveButton(getString(R.string.txt_confirm), (dialogInterface, i) -> {
                    chosenCategory = null;
                    dialogInterface.dismiss();
                    reloadPosts();
                });

        androidx.appcompat.app.AlertDialog dg = dialogBuilder.create();
        dg.setOnShowListener(dialogInterface -> {
            TypedValue typedValue1 = new TypedValue();
            Resources.Theme theme = getTheme();
            theme.resolveAttribute(R.attr.colorSecondary, typedValue1, true);
            dg.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue1.data);
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                userPreferences.putString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS, Integer.toString(i));
                textView.setText(getString(R.string.txt_radius) + ": " + userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS) + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        dg.show();

    }

    /**
     * Reloads all posts in a specific area from Firebase.
     */
    private void reloadPosts() {
        posts.removeAll(posts);
        txtNoPostFound.setVisibility(View.INVISIBLE);
        if(adapter != null)
            adapter.notifyDataSetChanged();
        if(location != null) {
            mainProgressBar.setVisibility(View.VISIBLE);
            txtLoadingPosts.setText(getString(R.string.loading_posts).concat(" " + userPreferences.getString(UserPreferences.SHARED_PREFERENCES_SEARCH_RADIUS) + " km..."));
            txtLoadingPosts.setVisibility(View.VISIBLE);
        } else {
            mainProgressBar.setVisibility(View.VISIBLE);
            txtLoadingLocation.setVisibility(View.VISIBLE);
        }
        getAllPosts();
    }

    /**
     * Redirects user to chat activity belonging to specific post.
     * @param postId The id of the post
     * @param postTitle Title of the post
     */
    public void openPostChat(String postId, String postTitle){
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("POST_ID", postId);
        intent.putExtra("POST_TITLE", postTitle);
        startActivityForResult(intent, POST_CHAT_REQUEST_CODE);
    }

    /**
     * Shows popup for selecting desired post category.
     * @param v
     */
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(item -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.choose_category)
                    .setView(getLayoutInflater().inflate(R.layout.categorydialog_message, null));
            builder.setPositiveButton(R.string.ok, (dialog, id) -> {
                if(chosenCategory != null) {
                    reloadPosts();
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton(R.string.cancel, (dialog, id) -> chosenCategory = null);

            AlertDialog dialog = builder.create();
            dialog.setOnShowListener(arg0 -> {
                TypedValue typedValue = new TypedValue();
                Resources.Theme theme = getTheme();
                theme.resolveAttribute(R.attr.colorSecondary, typedValue, true);
                int color = typedValue.data;
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.TRANSPARENT);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(Color.TRANSPARENT);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(color);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(color);
            });
            dialog.show();
            return false;
        });
        popup.inflate(R.menu.main);
        popup.show();
    }

    /**
     * Selects desired category.
     * @param view
     */
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_social:
                if (checked) { chosenCategory = Integer.toString(0); break; }
            case R.id.radio_restaurant:
                if (checked) { chosenCategory = Integer.toString(1); break; }
            case R.id.radio_education:
                if (checked) { chosenCategory = Integer.toString(2); break; }
            case R.id.radio_entertainment:
                if (checked) { chosenCategory = Integer.toString(3); break; }
            case R.id.radio_dating:
                if (checked) { chosenCategory = Integer.toString(4); break; }
            case R.id.radio_other:
                if (checked) { chosenCategory = Integer.toString(5); break; }
        }
    }

    /**
     * Opens alert dialog for reporting posts.
     * @param view
     */
    public void reportPost(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle(R.string.describe_problem)
                .setView(getLayoutInflater().inflate(R.layout.reportdialog_message, null));

        builder.setPositiveButton(R.string.ok, (dialog, id) -> {
            if(isReportCheckBoxChecked) {
                dialog.dismiss();
                Toast.makeText(MainActivity.this, getString(R.string.report_sent), Toast.LENGTH_LONG).show();
                isReportCheckBoxChecked = false;
                checkBoxCounter = 0;
            }
        });
        builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
            isReportCheckBoxChecked = false;
            checkBoxCounter = 0;
        });
        builder.setOnDismissListener(dialogInterface -> {
            isReportCheckBoxChecked = false;
            checkBoxCounter = 0;
        });
        builder.setOnCancelListener(dialogInterface -> {
            isReportCheckBoxChecked = false;
            checkBoxCounter = 0;
        });

        AlertDialog dialog = builder.create();

        dialog.setOnShowListener(arg0 -> {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getTheme();
            theme.resolveAttribute(R.attr.colorSecondary, typedValue, true);
            int color = typedValue.data;
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.TRANSPARENT);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(Color.TRANSPARENT);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(color);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(color);
        });
        dialog.show();

    }

    /**
     * Activates when a report reason in selected.
     * @param view
     */
    public void onCheckBoxClicked(View view) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorSecondary, typedValue, true);
        ColorStateList colorStateList = new ColorStateList(
                new int[][] {
                        new int[] { -android.R.attr.state_checked }, // unchecked
                        new int[] {  android.R.attr.state_checked }  // checked
                },
                new int[] {
                        Color.GRAY,
                        typedValue.data
                }
        );
        ((AppCompatCheckBox) view).setSupportButtonTintList(colorStateList);
        isReportCheckBoxChecked = ((CheckBox) view).isChecked();
        if(isReportCheckBoxChecked) {
            checkBoxCounter++;
            isReportCheckBoxChecked = true;
        } else {
            checkBoxCounter--;
            if(checkBoxCounter == 0)
                isReportCheckBoxChecked = false;
            else
                isReportCheckBoxChecked = true;
        }
    }


    /**
     * Async task for downloading user profile pic from Firebase URI.
     */
    private static class DownloadBitmapFromUri extends AsyncTask<Uri, Void, Bitmap> {

        private WeakReference<Context> activityReference;

        DownloadBitmapFromUri(Context context){
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Bitmap doInBackground(Uri... uris) {
            Bitmap bmp = null;
            try {
                URL ulrn = new URL(uris[0].toString());
                HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
                InputStream is = con.getInputStream();
                bmp = BitmapFactory.decodeStream(is);
                if (null != bmp){
                    bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
                    return bmp;
                }


            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, e.getMessage());
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            MainActivity activity = (MainActivity)activityReference.get();
            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), bitmap);
            roundedBitmapDrawable.setCornerRadius(150);
            activity.displayProfilePic.setImageDrawable(roundedBitmapDrawable);
        }
    }

}
