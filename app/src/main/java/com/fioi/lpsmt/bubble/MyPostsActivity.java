package com.fioi.lpsmt.bubble;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.fioi.lpsmt.bubble.helpers.PostDeleteScheduler;
import com.fioi.lpsmt.bubble.helpers.adapters.SinglePostAdapter;
import com.fioi.lpsmt.bubble.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrListener;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

public class MyPostsActivity extends AppCompatActivity {
    private static final String TAG = MyPostsActivity.class.getSimpleName();

    private SinglePostAdapter adapter;
    public List<Post> posts;
    private RecyclerView myPostsView;
    private ProgressBar myPostsProgressBar;
    private LinearLayoutManager listViewManager;
    private Paint p = new Paint();
    private TextView swipeToRemove;
    private TextView stillNoPostHere;
    private boolean shouldRefresh = false;
    private PostDeleteScheduler postDeleteScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myposts);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            if (shouldRefresh) {
                setResult(RESULT_OK);
            } else {
                setResult(RESULT_CANCELED);
            }
            finish();
        });

        myPostsView = findViewById(R.id.my_posts_list);
        myPostsProgressBar = findViewById(R.id.myPostsProgressBar);
        myPostsProgressBar.setVisibility(View.GONE);
        listViewManager = new LinearLayoutManager(MyPostsActivity.this);
        myPostsView.setLayoutManager(listViewManager);
        posts = new ArrayList<>();
        swipeToRemove = findViewById(R.id.swipe_to_remove);
        swipeToRemove.setVisibility(View.GONE);
        stillNoPostHere = findViewById(R.id.still_no_post_here);
        stillNoPostHere.setVisibility(View.GONE);
        postDeleteScheduler = new PostDeleteScheduler();

        adapter = new SinglePostAdapter(posts);
        myPostsView.setAdapter(adapter);

        Slidr.attach(this, configSlidr());
        loadMyPosts();
        enableSwipe();
    }

    /**
     * Builds slider for transitioning between activities.
     * @return SlidrConfig instance.
     */
    private SlidrConfig configSlidr() {
        return new SlidrConfig.Builder()
                .listener(new SlidrListener() {
                    @Override
                    public void onSlideStateChanged(int state) {

                    }

                    @Override
                    public void onSlideChange(float percent) {

                    }

                    @Override
                    public void onSlideOpened() {

                    }

                    @Override
                    public void onSlideClosed() {
                        if (shouldRefresh) {
                            setResult(RESULT_OK);
                        } else {
                            setResult(RESULT_CANCELED);
                        }
                        finish();
                    }
                }).build();
    }

    @Override
    public void onBackPressed() {
        if (shouldRefresh) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    /**
     * Handles swipe actions
     */
    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                    final Post deletedPost = posts.get(position);
                    final int deletedPosition = position;
                    adapter.removeItem(position);
                    postDeleteScheduler.getDeletedPostIds().add(deletedPost.getId());
                    shouldRefresh = true;

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.my_posts_list), deletedPost.getTitle() + " " + getString(R.string.deleted), Snackbar.LENGTH_LONG);
                    snackbar.setAction(getString(R.string.undo), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            adapter.restoreItem(deletedPost, deletedPosition);
                            postDeleteScheduler.getDeletedPostIds().remove(deletedPost.getId());
                            if(postDeleteScheduler.getDeletedPostIds().size() == 0)
                                shouldRefresh = false;
                        }
                    });
                    snackbar.setActionTextColor(Color.YELLOW);
                    snackbar.show();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(myPostsView);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop(){
        super.onStop();
        if(postDeleteScheduler.getDeletedPostIds().size() > 0){
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            WriteBatch batch = db.batch();
            for(String postId : postDeleteScheduler.getDeletedPostIds()){
                batch.delete(db.collection("posts").document(postId));
            }
            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "Successfully deleted posts.");
                    MainActivity.postDeleteListener.setValue("Post deleted " + System.currentTimeMillis());

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "Failed to delete posts.", e);
                }
            });
        }
    }

    /**
     * Loads current user's posts.
     */
    private void loadMyPosts() {
        myPostsProgressBar.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("posts").whereEqualTo("user", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .orderBy("createdAt", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.d(TAG, "There was an error in getting the posts", e);
                }
                posts.clear();
                if(queryDocumentSnapshots != null && queryDocumentSnapshots.isEmpty()) {
                    swipeToRemove.setVisibility(View.INVISIBLE);
                    stillNoPostHere.setVisibility(View.VISIBLE);
                } else {
                    stillNoPostHere.setVisibility(View.INVISIBLE);
                    swipeToRemove.setVisibility(View.VISIBLE);
                    if(queryDocumentSnapshots != null){
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            Post post = new Post();
                            post.setId(documentSnapshot.getId());
                            post.setBody(documentSnapshot.getString("body"));
                            post.setTitle(documentSnapshot.getString("title"));
                            post.setExpiration(documentSnapshot.getDate("expiration"));
                            posts.add(post);
                        }
                    }

                }

                adapter.notifyDataSetChanged();
                myPostsProgressBar.setVisibility(View.INVISIBLE);
            }
        });

    }
}
