package com.fioi.lpsmt.bubble.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import com.fioi.lpsmt.bubble.MainActivity;
import com.fioi.lpsmt.bubble.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MESSAGING_SERVICE";
    private final String ADMIN_CHANNEL_ID ="admin_channel";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String postId = remoteMessage.getData().get("postId");
        String postTitle = remoteMessage.getData().get("postTitle");
        String notificationTitle = remoteMessage.getData().get("title");
        String notificationBody = remoteMessage.getData().get("body");
        String clickAction = remoteMessage.getData().get("click_action");

        Intent main = new Intent(this, MainActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);

        final Intent intent = new Intent(clickAction);
        intent.putExtra("POST_ID", postId);
        intent.putExtra("POST_TITLE", postTitle);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        int notificationID = postId.hashCode();

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        /*
          Apps targeting SDK 26 or above (Android O) must implement notification channels and add its notifications
          to at least one of them. Therefore, confirm if version is Oreo or higher, then setup notification channel
        */
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels(manager);
        }

        Intent[] intents = new Intent[]{main, intent};
        PendingIntent pendingIntent = PendingIntent.getActivities(this, notificationID, intents, PendingIntent.FLAG_ONE_SHOT);

        Bitmap largeIcon;
        try {
            URL url = new URL(remoteMessage.getData().get("senderImage"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            largeIcon = BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            e.printStackTrace();
        }

        Resources res = MyFirebaseMessagingService.this.getResources();
        Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder groupNotificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(RoundedBitmapDrawableFactory.create(getResources(), largeIcon).getBitmap())
                .setContentTitle(notificationTitle)
                .setContentText(notificationBody)
                .setStyle(new NotificationCompat.InboxStyle().setSummaryText(getString(R.string.pref_title_new_message_notifications)))
                .setGroup("com.fioi.lpsmt.bubble")
                .setGroupSummary(true)
                .setSound(notificationSoundUri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(RoundedBitmapDrawableFactory.create(getResources(), largeIcon).getBitmap())
                .setContentTitle(notificationTitle)
                .setContentText(notificationBody)
                .setGroup("com.fioi.lpsmt.bubble")
                .setSound(notificationSoundUri)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        //Set notification color to match your app color template
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimaryLight));
        }

        manager.notify(1, groupNotificationBuilder.build());
        manager.notify(notificationID, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(NotificationManagerCompat notificationManager){
        CharSequence adminChannelName = "New notification";
        String adminChannelDescription = "Device to device notification";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

}
