package com.fioi.lpsmt.bubble.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private Pattern pattern;
    private Matcher matcher;

    public static final String EMAIL_PATTERN = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

    public Validator(String patternToValidate) {
        pattern = Pattern.compile(patternToValidate, Pattern.CASE_INSENSITIVE);
    }

    public boolean validate(final String param) {

        matcher = pattern.matcher(param);
        return matcher.matches();

    }
}
