package com.fioi.lpsmt.bubble.helpers.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.fioi.lpsmt.bubble.ChatActivity;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.model.Message;

import java.util.List;

/**
 * RecyclerView adapter for personal chat area.
 */
public class MyMessagesAdapter extends RecyclerView.Adapter<MyMessagesAdapter.ViewHolder> {
    private final List<Message> messages;

    public MyMessagesAdapter(List<Message> messages) {
        this.messages = messages;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new MyMessagesAdapter.ViewHolder(inflater.inflate(R.layout.list_item_mymessages, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.title_post.setText(message.getPostName());
        holder.name_sender.setText(message.getSenderName() + ":");
        holder.message_sent.setText(message.getText());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title_post, name_sender, message_sent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title_post = itemView.findViewById(R.id.title_post);
            name_sender = itemView.findViewById(R.id.name_sender);
            message_sent = itemView.findViewById(R.id.message_sent);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(view.getContext(), ChatActivity.class);
                    intent.putExtra("POST_ID", messages.get(position).getPost());
                    intent.putExtra("POST_TITLE", messages.get(position).getPostName());
                    view.getContext().startActivity(intent);
                }
            });
        }
    }

}
