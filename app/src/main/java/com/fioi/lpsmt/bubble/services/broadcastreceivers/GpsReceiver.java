package com.fioi.lpsmt.bubble.services.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import com.fioi.lpsmt.bubble.helpers.LocationCallback;


/**
 * Handles operations regarding Location Services (Checks whether user turned it on/off.)
 */
public class GpsReceiver extends BroadcastReceiver {
    private final LocationCallback locationCallback;

    public GpsReceiver(LocationCallback locationCallback){
        this.locationCallback = locationCallback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        LocationManager locationManager =  (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        String action = intent.getAction();
        if(action != null && action.matches("android.location.PROVIDERS_CHANGED")){
            boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if(!(gpsEnabled && networkEnabled)){
                locationCallback.onLocationTriggered();
            }
        }

    }
}
