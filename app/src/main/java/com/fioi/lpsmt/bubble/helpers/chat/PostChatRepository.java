package com.fioi.lpsmt.bubble.helpers.chat;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles instant messaging.
 */
public class PostChatRepository {
    private static final String TAG = "POST_CHAT_REPOSITORY";

    private FirebaseFirestore db;

    public PostChatRepository(FirebaseFirestore db){
        this.db = db;
    }

    /**
     * Sends new message to post chat room.
     * @param postId id of the post that the message will be associated to.
     * @param senderId id of the sender.
     * @param message message model.
     * @param senderName name of the sender.
     * @param senderImageUrl profile pic of the sender.
     * @param postName name of the post that the message will be associated to.
     * @param successCallback success callback
     * @param failureCallback failure callback
     */
    public void sendMessage(String postId, String senderId,
                            String message, String senderName,
                            String senderImageUrl, String postName,
                            final OnSuccessListener<DocumentReference> successCallback,
                            final OnFailureListener failureCallback){
        Map<String, Object> messageData = new HashMap<>();
        messageData.put("post", postId);
        messageData.put("sender", senderId);
        messageData.put("text", message);
        messageData.put("sent", System.currentTimeMillis());
        messageData.put("senderName", senderName);
        messageData.put("senderImageUrl", senderImageUrl);
        messageData.put("postName", postName);

        db.collection("messages")
                .add(messageData)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        successCallback.onSuccess(documentReference);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        failureCallback.onFailure(e);
                    }
                });
    }


    /**
     * Returns all messages associated to specific post.
     * @param postId the id of the post whose messages will be fetched.
     * @param listener listener enabling real time messaging.
     */
    public void getPostMessages(String postId, EventListener<QuerySnapshot> listener){
        db.collection("messages")
                .whereEqualTo("post", postId)
                .orderBy("sent", Query.Direction.DESCENDING)
                .addSnapshotListener(listener);
    }
}
