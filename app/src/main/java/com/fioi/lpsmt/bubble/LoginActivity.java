package com.fioi.lpsmt.bubble;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fioi.lpsmt.bubble.fragments.PasswordLinkDialog;
import com.fioi.lpsmt.bubble.helpers.SocialDisconnectHelper;
import com.fioi.lpsmt.bubble.helpers.UserPreferences;
import com.fioi.lpsmt.bubble.recovery.PasswordRecovery;
import com.fioi.lpsmt.bubble.recovery.ResetPassword;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements PasswordLinkDialog.PasswordLinkDialogListener{
    private static final String TAG = "LOGIN_ACTIVITY";
    private static int GOOGLE_SIGN_ID = 101;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private HashMap<String, Object> userMap;

    private LoginButton facebookLoginButton;
    private CallbackManager callbackManager;
    private AuthCredential facebookCredentials= null;
    private static final String EMAIL = "email";
    private static final String FB_PROFILE = "public_profile";

    public GoogleSignInClient googleSignInClient;
    private SignInButton googleLoginButton;
    private GoogleSignInAccount googleSignInAccount;

    private EditText emailField;
    private EditText passwordField;
    private ProgressBar loginProgressBar;
    private UserPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackManager =  CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle messages = getIntent().getExtras();
        if(messages != null){
            Toast toast = Toast.makeText(this, messages.getString(ResetPassword.RESET_PASS_ERROR), Toast.LENGTH_LONG);
            toast.show();
        }

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
        userMap = new HashMap<>();

        loginProgressBar = findViewById(R.id.loginProgressBar);
        loginProgressBar.setVisibility(View.GONE);

        facebookLoginButton = findViewById(R.id.login_button);
        facebookLoginButton.setPermissions(Arrays.asList(EMAIL, FB_PROFILE));

        /**
         * Get user data from Facebook.
         */
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                userPreferences.putString(UserPreferences.USER_AUTH_TYPE, "facebook.com");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if(response.getError() != null){
                            Log.d(TAG, "Error fetching user data");
                        }else{
                            userPreferences.putString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_ID, object.optString("id"));
                            userPreferences.putString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_EMAIL, object.optString("email"));
                            userPreferences.putString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_NAME, object.optString("name"));
                            String profilePictureUrl = null;
                            try {
                                profilePictureUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                            } catch (JSONException e) {
                                Log.d(TAG, e.getMessage());
                                e.printStackTrace();
                            }
                            userPreferences.putString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_PHOTO_URL, profilePictureUrl);
                            firebaseAuthWithFacebook(loginResult.getAccessToken());
                        }
                    }
                });
                Bundle params = new Bundle();
                params.putString("fields", "id, name, email, picture.type(large)");
                request.setParameters(params);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                Log.d("FACEBOOK_LOGIN_CANCEL", "Login was cancelled!");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("FACEBOOK_EXCEPTION", error.getMessage());
                error.printStackTrace();
            }
        });

        googleLoginButton = findViewById(R.id.google_login_button);
        googleLoginButton.setSize(SignInButton.SIZE_WIDE);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        googleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Logged in with google!");
                userPreferences.putString(UserPreferences.USER_AUTH_TYPE, "google.com");
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, GOOGLE_SIGN_ID);
            }
        });

        emailField = findViewById(R.id.login_email);
        passwordField = findViewById(R.id.login_password);
        userPreferences = new UserPreferences(this);
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }

    /*-----------------------------------------Google-------------------------------------*/
    /**
     * Activates when user returns to login activity with a result from other activities.
     * Used to get login data from Google.
     * @param requestCode Request code of operation.
     * @param resultCode Result code of operation.
     * @param data Data passed from other activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GOOGLE_SIGN_ID){
            if(resultCode == Activity.RESULT_OK){
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }
        }
    }

    /**
     * Handles data returned from Google.
     * @param completedTask completedTask returned from google with user account data.
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask){
        try {
            googleSignInAccount = completedTask.getResult(ApiException.class);
            if (googleSignInAccount != null) {
                firebaseAuthWithGoogle(googleSignInAccount);
            }
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Toast toast = Toast.makeText(this, getString(R.string.error_sign_in_google), Toast.LENGTH_LONG);
            toast.show();
        }
    }
    /*----------------------------------------Firebase---------------------------------------------*/

    /**
     * Adds user data from Google to Firebase and authenticates user.
     * @param acct GoogleSignInAccount containing user data.
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        loginProgressBar.setVisibility(View.VISIBLE);
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser authenticatedUser = mAuth.getCurrentUser();
                            if(facebookCredentials != null && authenticatedUser != null){
                                authenticatedUser.linkWithCredential(facebookCredentials);
                            }
                            userMap.put("Name", acct.getDisplayName());
                            userMap.put("Email", acct.getEmail());
                            userMap.put("PhotoUrl", acct.getPhotoUrl().toString());
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    Log.d(TAG, "Successfully got instance id");
                                    userMap.put("deviceToken", instanceIdResult.getToken());
                                    db.collection("users").document(authenticatedUser.getUid()).set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(LoginActivity.TAG, "Document snapshot successfully written");
                                            loginProgressBar.setVisibility(View.INVISIBLE);
                                            successfulAuthentication();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e(LoginActivity.TAG, "Failed to write document snapshot");
                                            loginProgressBar.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG, "Failed to save device token");
                                    loginProgressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            loginProgressBar.setVisibility(View.INVISIBLE);
                            Toast toast = Toast.makeText(LoginActivity.this, getString(R.string.error_sign_in_google), Toast.LENGTH_LONG);
                            toast.show();
                        }


                    }
                });
    }

    /**
     * Adds user data from Facebook to Firebase and authenticates user.
     * @param token access token received from Facebook.
     */
    private void firebaseAuthWithFacebook(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        loginProgressBar.setVisibility(View.VISIBLE);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Credential login succeeded (no new account)");
                            FirebaseUser authenticatedUser = mAuth.getCurrentUser();
                            userMap.put("Name", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_NAME));
                            userMap.put("Email", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_EMAIL));
                            userMap.put("PhotoUrl", userPreferences.getString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_PHOTO_URL));
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    Log.d(TAG, "Successfully got instance id");
                                    userMap.put("deviceToken", instanceIdResult.getToken());
                                    db.collection("users").document(authenticatedUser.getUid()).set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(LoginActivity.TAG, "Document snapshot successfully written");
                                            loginProgressBar.setVisibility(View.INVISIBLE);
                                            successfulAuthentication();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e(LoginActivity.TAG, "Failed to write document snapshot");
                                            loginProgressBar.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG, "Failed to save device token");
                                    loginProgressBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        } else {
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                FirebaseAuthUserCollisionException exception = (FirebaseAuthUserCollisionException)task.getException();
                                if(exception.getErrorCode().equals("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL")){
                                    mAuth.fetchSignInMethodsForEmail(userPreferences.getString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_EMAIL)).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                                            if(task.isSuccessful()){
                                                if(task.getResult().getSignInMethods().contains(GoogleAuthProvider.PROVIDER_ID)){
                                                    activateFacebookLink(credential);
                                                    Intent googleSignInIntent = googleSignInClient.getSignInIntent();
                                                    startActivityForResult(googleSignInIntent, GOOGLE_SIGN_ID);
                                                }else if(task.getResult().getSignInMethods().contains(EmailAuthProvider.PROVIDER_ID)){
                                                    activateFacebookLink(credential);
                                                    openPassLinkDialog();
                                                }else{
                                                    SocialDisconnectHelper.disconnectFromFacebook(userPreferences);
                                                    Toast.makeText(LoginActivity.this, getString(R.string.error_login), Toast.LENGTH_LONG).show();
                                                }
                                            }else{
                                                Log.d(TAG, task.getException().getMessage());
                                                failureAuthentication();
                                            }
                                        }
                                    });
                                }
                            }

                        }
                    }
                });
    }

    /**
     * Used if authentication requires account linking.
     * @param facebookCredentials Firebase Facebook credentials.
     */
    private void activateFacebookLink(AuthCredential facebookCredentials) {
        this.facebookCredentials = facebookCredentials;
    }


    /**
     * Authenticates user to Firebase with email and password.
     * @param view
     */
    public void firebaseAuthWithEmailPass(View view) {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();
        if(!email.equals("") && !password.equals("")){
            loginProgressBar.setVisibility(View.VISIBLE);
            userPreferences.putString(UserPreferences.USER_AUTH_TYPE, "bubble.local");
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "signInWithEmail:success");

                                FirebaseUser currentUser = mAuth.getCurrentUser();

                                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                                    @Override
                                    public void onSuccess(InstanceIdResult instanceIdResult) {
                                        Log.d(TAG, "Successfully got instance id");

                                        String deviceToken = instanceIdResult.getToken();
                                        db.collection("users").document(currentUser.getUid()).update("deviceToken", deviceToken).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d(TAG, "Successfully saved device token");
                                                loginProgressBar.setVisibility(View.INVISIBLE);
                                                successfulAuthentication();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d(TAG, "Failed to save device token");
                                                loginProgressBar.setVisibility(View.INVISIBLE);
                                            }
                                        });

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d(TAG, "Failed to get instance id");
                                        loginProgressBar.setVisibility(View.INVISIBLE);
                                    }
                                });


                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                loginProgressBar.setVisibility(View.INVISIBLE);
                                failureAuthentication();
                            }
                        }
                    });
        }else{
            Toast.makeText(this, getString(R.string.enter_email_password), Toast.LENGTH_LONG).show();
        }

    }


    /**
     * Redirects user to Register Activity.
     * @param view
     */
    public void openRegisterActivity(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }


    /**
     * Activates when authentication procedure successfully terminates and redirects user to Main Activity.
     */
    private void successfulAuthentication(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Activates when authentication fails.
     */
    private void failureAuthentication() {
        Toast.makeText(this, getString(R.string.wrong_email_password), Toast.LENGTH_LONG).show();
    }

    /**
     * Redirects user to Password Recovery Activity.
     * @param view
     */
    public void onForgotPassword(View view) {
        Intent intent = new Intent(LoginActivity.this, PasswordRecovery.class);
        startActivity(intent);
    }

    /*-------------------------------------------Confirm Facebook Link-----------------------------*/

    /**
     * Activates when Facebook authentication requires account linking. Opens dialog to enter password.
     */
    private void openPassLinkDialog() {
        PasswordLinkDialog passwordLinkDialog = new PasswordLinkDialog();
        passwordLinkDialog.show(getSupportFragmentManager(), "Confirm Account");

    }

    /**
     * Activates when Facebook authentication requires account linking.
     */
    @Override
    public void returnPassword(String password) {
        mAuth.signInWithEmailAndPassword(userPreferences.getString(UserPreferences.SHARED_PREFERENCES_FACEBOOK_EMAIL), password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            mAuth.getCurrentUser().linkWithCredential(facebookCredentials);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            failureAuthentication();
                        }
                    }
                });
    }
}
