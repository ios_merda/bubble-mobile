package com.fioi.lpsmt.bubble.helpers.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.fioi.lpsmt.bubble.R;
import com.fioi.lpsmt.bubble.model.Message;
import com.makeramen.roundedimageview.RoundedImageView;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Recycler view adapter for chat view.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {
    private static final int SENT = 0;
    private static final int RECEIVED = 1;

    private String userId;
    private List<Message> messages;
    int status = SENT;


    public ChatAdapter(String userId, List<Message> messages){
        this.userId = userId;
        this.messages = messages;
    }


    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        if(viewType == SENT){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_sent, parent, false);
        }else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_received, parent, false);
        }

        return new ChatViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        holder.bind(messages.get(position));
    }

    @Override
    public int getItemViewType(int position){
        if(messages.get(position).getSender().contentEquals(userId)){
            status = SENT;
            return SENT;
        }else{
            status = RECEIVED;
            return RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder{
        TextView messageTextView;
        TextView senderName;
        TextView time;
        RoundedImageView senderImageView;

        public ChatViewHolder(View itemView){
            super(itemView);
            messageTextView = itemView.findViewById(R.id.chat_message);
            senderImageView = itemView.findViewById(R.id.imageViewSender);
            time = itemView.findViewById(R.id.time);
            if(status == RECEIVED){
                senderName = itemView.findViewById(R.id.name);
            }

        }

        public void bind(Message message){
            messageTextView.setText(message.getText());
            if(status == RECEIVED){
                senderName.setText(message.getSenderName());
            }
            long timeSent = message.getSent();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm");

            Date resultdate = new Date(timeSent);
            time.setText(sdf.format(resultdate));
            Glide.with(senderImageView)
                    .load(message.getSenderImage())
                    .into(senderImageView);

        }
    }
}
